import { extendTheme } from '@chakra-ui/react'

const theme = extendTheme({
  colors: {
    primary: {
      50: "#e6e3ef",
      100: "#8477b0",
      400: "#322659",
      900: "#1e1735",
      1000: "#0c0915"
    },
  },
})

export default theme