// import { ReactElement } from 'react';
import { Box, SimpleGrid, Icon, Heading, Stack, Flex, useColorModeValue } from '@chakra-ui/react';
import { FcDonate } from 'react-icons/fc';
import { FaTrophy } from 'react-icons/fa';

// interface FeatureProps {
//   title: string;
//   text: string;
//   icon: ReactElement;
// }

const Feature = ({ title, text, icon }) => {
	return (
		<Stack >
			<Flex
				w={16}
				h={[0, 16]}
				align={'center'}
				justify={'center'}
				color={useColorModeValue('primary.400', 'primary.50')}
				rounded={'full'}
				// bg={'gray.100'}
				mb={1}>
				{/* {icon} */}
				<Heading size="lg" fontWeight={600}>{title}</Heading>
			</Flex>
			{/* <Text color={useColorModeValue("grey.600", "grey.300")}>{text}</Text> */}
		</Stack>
	);
};

export default function HomePageFeature() {
	return (
		<Box p={16} bg={useColorModeValue("primary.50", "primary.900")}>
			<Box p={8} >
				<SimpleGrid columns={{ base: 1, md: 5 }} spacing={{ base: 20, md: 40 }}>
					<Feature
						icon={<Icon as={FaTrophy} color={'primary.400'} w={10} h={10} />}
						title={'16 Teams'}
					/>
					<Feature
						icon={<Icon as={FcDonate} w={10} h={10} />}
						title={'7 Players'}

					/>
					<Feature
						icon={<Icon as={FcDonate} w={10} h={10} />}
						title={'5 Games'}
					/>
					<Feature
						icon={<Icon as={FcDonate} w={10} h={10} />}
						title={'1 Final'}

					/>
					<Feature
						icon={<Icon as={FcDonate} w={10} h={10} />}
						title={'1 Cup'}

					/>
				</SimpleGrid>
			</Box>
		</Box>
	);
}