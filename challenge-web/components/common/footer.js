import { ReactNode } from 'react';
import {
	Box,
	Container,
	Stack,
	SimpleGrid,
	Text,
	// Link,
	VisuallyHidden,
	chakra,
	useColorModeValue,
} from '@chakra-ui/react';
import { FaTwitter, FaYoutube, FaInstagram } from 'react-icons/fa';

import Link from 'next/link'
// import AppStoreBadge from '@/components/AppStoreBadge';
// import PlayStoreBadge from '@/components/PlayStoreBadge';

const ListHeader = ({ children }) => {
	return (
		<Text fontWeight={'500'} fontSize={'lg'} mb={2}>
			{children}
		</Text>
	);
};

const SocialButton = ({
	children,
	label,
	href,
}) => {
	return (
		<chakra.button
			bg={useColorModeValue('blackAlpha.100', 'whiteAlpha.100')}
			rounded={'full'}
			w={8}
			h={8}
			cursor={'pointer'}
			as={'a'}
			href={href}
			display={'inline-flex'}
			alignItems={'center'}
			justifyContent={'center'}
			transition={'background 0.3s ease'}
			_hover={{
				bg: useColorModeValue('blackAlpha.200', 'whiteAlpha.200'),
			}}>
			<VisuallyHidden>{label}</VisuallyHidden>
			{children}
		</chakra.button>
	);
};

export default function Footer() {
	return (
		<Box
			bg={useColorModeValue('primary.50', 'primary.900')}
			color={useColorModeValue('gray.700', 'gray.200')}>
			<Container as={Stack} maxW={'6xl'} py={10}>
				<SimpleGrid columns={{ base: 1, sm: 2, md: 3 }} spacing={8}>
					<Stack align={'flex-start'}>
						<ListHeader>Competition</ListHeader>
						<Link href={'/about'}>About</Link>
						<Link href={'/results'}>Results</Link>
						{/* <Link href={'/live'}>Live</Link> */}
					</Stack>

					<Stack align={'flex-start'}>
						<ListHeader>Arranmore Island</ListHeader>
						<Link href={'https://seoarainnmhor.com'}>Seo Árainn Mhór</Link>
						<Link href={'https://thearranmoreferry.com'}>Arranmore Blue Ferry</Link>
						<Link href={'https://arranmoreferry.com'}>Arranmore Red Ferry</Link>
						{/* <Link href={'https://'}>Community Guidelines</Link> */}
					</Stack>

					<Stack align={'flex-start'}>
						<ListHeader>Install App</ListHeader>
						<Text>App Coming Soon...</Text>
						{/* <AppStoreBadge />
						<PlayStoreBadge /> */}
					</Stack>
				</SimpleGrid>
			</Container>

			<Box
				borderTopWidth={1}
				borderStyle={'solid'}
				borderColor={useColorModeValue('gray.200', 'gray.700')}>
				<Container
					as={Stack}
					maxW={'6xl'}
					py={4}
					direction={{ base: 'column', md: 'row' }}
					spacing={4}
					justify={{ md: 'space-between' }}
					align={{ md: 'center' }}>
					<Text>© {`${new Date().getFullYear()}`} Hexa Studios. All rights reserved</Text>
					<Stack direction={'row'} spacing={6}>
						<SocialButton label={'Twitter'} href={'https://twitter.com/ArranChallenge'}>
							<FaTwitter />
						</SocialButton>
						<SocialButton label={'Instagram'} href={'https://www.instagram.com/arranchallenge/'}>
							<FaInstagram />
						</SocialButton>
					</Stack>
				</Container>
			</Box>
		</Box>
	);
}