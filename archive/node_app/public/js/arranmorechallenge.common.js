// COMMON FUNCTIONS

var endpoint = "https://live.arranmorechallenge.com";
// var endpoint = "http://localhost:2017";
var refreshRate = 60*1000;


// setInterval(function() {
//     // your code goes here...
// }, 60 * 1000);


loadMessages();

function loadMessages(){
	$.get( endpoint+"/updates", function( data ) {
		// console.log("initial load");
		// console.log(data);
		if(data.messages.length == 0 ){
			$('#messagesContainer').html('<div class="align-center">There are no messages at this time.</div>');
			$('#oldestMessageBar').hide();
		} else {
			// do nothing yet
			for (var i = data.messages.length - 1; i >= 0; i--) {
				// console.log(data.messages[i]);
				if (data.messages[i].adminStatus == "true"){
					var adminMessageHTML = constructAdminMessage(data.messages[i])
					/// prepend to messages
					$("#messagesContainer").prepend(adminMessageHTML);
				} else {
					var fanMessageHTML = constructFanMessage(data.messages[i])
					$("#messagesContainer").prepend(fanMessageHTML);
				}
				// if (i >= 1){
				// 	// data.messages[i]
				// 	$('#oldestMessageTime').val(data.messages[i].timestamp);
				// }
			}

			$('#oldestMessageBar').show();
			// HIDE THE LATEST MESSAGE ID
			$('#latestMessageTime').val(data.messages[0].timestamp);
			// OLDEST MESSAGE
			$('#oldestMessageTime').val(data.messages[data.messages.length-1].timestamp);

		}
	});
};

function constructAdminMessage(messageObj){

	var html = '<div class="unit w-2-3 card plain card-message admin">';
	html += '<div class="profile-icon" id="profile_'+messageObj.userID+'"></div>';
	// if (messageObj.userProfileImage){
	// 	html += '<img src="'+messageObj.userProfileImage+'" />';
	// }

	var timestamp = getTimeString(messageObj.timestamp);

	html += '<div class="message-text-admin"><a href="/profile/'+messageObj.userID+'"><h3 class="username">'+messageObj.username+'</h3> </a><br>';
	html += '<span class="status">'+urlify(messageObj.content)+'</span></div>';
	html += '<div class="message-share-admin">';
	html += '<span class="message-timestamp">'+timestamp+'</span>';
	html += '<a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//live.arranmorechallenge.com/status/'+messageObj._id+'" target="_blank" class="message-share-button"><i class="icon-facebook"></i></a>';
	html += '<a href="https://twitter.com/home?status=http%3A//live.arranmorechallenge.com/status/'+messageObj._id+'" target="_blank" class="message-share-button"><i class="icon-twitter"></i></a> ';
	html += '<a href="/status/'+messageObj._id+'" target="_blank" class="message-share-button"><i class="icon-export"></i></a>';
	html += '</div></div>';

	return html

	// <div class="unit w-2-3 card plain card-message admin" >
	//     <div class="profile-icon"></div>
	//     <div class="message-text-admin">
	//         <a href="/profile/{id}"><h3 class="username">arranchallenge</h3> </a><br>
	//         <span class="status">This is the new format for the 2017 live Challenge update</span>
	//     </div>
	//     <div class="message-share-admin">
	//         <span class="message-timestamp">11:07am 15/01</span> <a href="" class="message-share-button"><i class="icon-facebook"></i></a> <a href="" class="message-share-button"><i class="icon-twitter"></i></a>  <a href="" class="message-share-button"><i class="icon-export"></i></a> 
	//     </div>
	// </div>

};

function constructFanMessage(messageObj){

	var html = '<div class="unit w-2-3 card plain card-message fan">';
	html += '<div class="profile-icon-fan" id="profile_'+messageObj.userID+'"></div>';
	// if (messageObj.userProfileImage){
	// 	html += '<img class="twitterImage" src="'+messageObj.userProfileImage+'" />';
	// }
	// console.log(messageObj.timestamp);

	var timestamp = getTimeString(messageObj.timestamp);

	html += '<div class="message-text-fan"><a href="/profile/'+messageObj.userID+'"><h3 class="username">'+messageObj.username+'</h3></a> <br>';
	html += '<span class="status">'+urlify(messageObj.content)+'</span> </div>';
	html += '<div class="message-share-fan">';
	html += '<a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//live.arranmorechallenge.com/status/'+messageObj._id+'" target="_blank" class="message-share-button"><i class="icon-facebook"></i></a>';
	html += '<a href="https://twitter.com/home?status=http%3A//live.arranmorechallenge.com/status/'+messageObj._id+'" target="_blank" class="message-share-button"><i class="icon-twitter"></i></a> ';
	html += '<a href="/status/'+messageObj._id+'" target="_blank" class="message-share-button"><i class="icon-export"></i></a>';
	html += '<span class="message-timestamp">'+timestamp+'</span>';
	html += '</div></div>';

	return html

    // <div class="unit w-2-3 card plain card-message fan" >
    //     <div class="profile-icon-fan"></div>
    //     <div class="message-text-fan">
    //         <a href="/profile/{id}"><h3 class="username">mat.loughnane</h3></a> <br>
    //         <span class="status">This is the new format for the 2017 live Challenge update</span>
    //     </div>
    //     <div class="message-share-fan">
    //         <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//live.arranmorechallenge.com/status/{id}" target="_blank" class="message-share-button"><i class="icon-facebook"></i></a> 
    //         <a href="https://twitter.com/home?status=http%3A//live.arranmorechallenge.com/status/{id}" target="_blank" class="message-share-button"><i class="icon-twitter"></i></a> 
    //         <a href="/status/{id}" target="_blank" class="message-share-button"><i class="icon-export"></i></a>
    //         <span class="message-timestamp">11:07am 15/01</span>
    //     </div> 
    // </div>
};

function getTimeString(epoch){
	var date = new Date(parseInt(epoch));
	// console.log(date);
	var hours = lessThanTen(date.getHours());
	var minutes = lessThanTen(date.getMinutes());

	var stringDate = lessThanTen(date.getDate());
	var stringMonth = lessThanTen(date.getMonth()+1);

	var string = hours+":"+minutes+" "+stringDate+"/"+stringMonth;
	return string;
}

function urlify(text) {
    // var urlRegex = /(https?:\/\/[^\s]+)/g;
    urlRegex = /(([a-z]+:\/\/)?(([a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel|local|internal))(:[0-9]{1,5})?(\/[a-z0-9_\-\.~]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&amp;]*)?)?(#[a-zA-Z0-9!$&'()*+.=-_~:@/?]*)?)(\s+|$)/gi;
    return text.replace(urlRegex, function(url) {
        return '<a href="' + url + '" target="_blank">' + url + '</a>';
    })
    // console.log("TESTING");
    // or alternatively
    // return text.replace(urlRegex, '<a href="$1">$1</a>')
}

function getPrettyTimeStamp(epoch){
	var date = new Date(parseInt(epoch));

	var hours = lessThanTen(date.getHours());
	var minutes = lessThanTen(date.getMinutes());

	var stringDate = lessThanTen(date.getDate());
	var stringMonth = lessThanTen(date.getMonth()+1);

	var string = hours+":"+minutes+" "+stringDate+"/"+stringMonth;
	return string;

}

function lessThanTen(number){
	if (number < 10){
		return "0"+number
	} else {
		return number
	}
}

$('#checkUpdates').click(function(){
	// console.log("check updates");
	// GET LATEST
	getLatestMessages();
});

function getLatestMessages(){
	var myCurrentTime = $('#latestMessageTime').val();

	console.log(myCurrentTime);

	if (typeof myCurrentTime == "latestMessageTime") {
		myCurrentTime = 0
	}

	$.get( endpoint+"/updates/latest/"+myCurrentTime, function( data ) {
		// console.log("initial load");
		// console.log(data);
		if(data.messages.length == 0 ){
			// $('#messagesContainer').html('<div class="align-center">There are no messages at this time.</div>');
		} else {
			// CLEAR MESSAGES
			// $('#messagesContainer').html('');
			// do nothing yet
			for (var i = data.messages.length - 1; i >= 0; i--) {
				// console.log(data.messages[i]);
				if (data.messages[i].adminStatus == "true"){
					var adminMessageHTML = constructAdminMessage(data.messages[i])
					/// prepend to messages
					$("#messagesContainer").prepend(adminMessageHTML);
				} else {
					var fanMessageHTML = constructFanMessage(data.messages[i])
					$("#messagesContainer").prepend(fanMessageHTML);
				}
				// 
				if (i >= 1){
				// data.messages[i]
					$('#oldestMessageTime').val(data.messages[i].timestamp);
				}
				// 
			}

			// HIDE THE LATEST MESSAGE ID
			if (data.messages.length > 0){
				$('#latestMessageTime').val(data.messages[0].timestamp);				
			}


		}
	});
};

$('#checkOlderUpdates').click(function(){
	// console.log("check updates");
	// GET LATEST
	var oldestMessageTime = $('#oldestMessageTime').val();
	getOlderMessages(oldestMessageTime);

});

function getOlderMessages(oldestMessageTime){

	// if (typeof oldestMessageTime == "string") {
	// 	oldestMessageTime = 0;
	// 	console.log("not a string");
	// }

	$.get( endpoint+"/updates/older/"+oldestMessageTime, function( data ) {
		// console.log("initial load");
		// console.log(data);
		if(data.messages.length == 0 ){
			$('#messagesContainer').append('<div style="clear:both"></div><div class="align-center">There are no more updates, start writing more!</div>');
			$('#oldestMessageBar').hide()
		} else {
			// CLEAR MESSAGES
			// $('#messagesContainer').html('');
			// do nothing yet
			for (var i = data.messages.length - 1; i >= 0; i--) {
				// console.log(data.messages[i]);
				if (data.messages[i].adminStatus == "true"){
					var adminMessageHTML = constructAdminMessage(data.messages[i])
					/// prepend to messages
					$("#messagesContainer").append(adminMessageHTML);
				} else {
					var fanMessageHTML = constructFanMessage(data.messages[i])
					$("#messagesContainer").append(fanMessageHTML);
				}
			}

			if (data.messages.length < 20){
				$('#messagesContainer').append('<div style="clear:both"></div><div class="align-center">There are no more updates, start writing more!</div>');
				$('#oldestMessageBar').hide();
			}

			// HIDE THE LATEST MESSAGE ID
			// if (data.messages.length > 0){
			// 	$('#latestMessageTime').val(data.messages[0].timestamp);				
			// }
			$('#oldestMessageTime').val(data.messages[data.messages.length-1].timestamp);


		}
	});
};
