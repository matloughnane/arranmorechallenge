import { parseISO, format } from 'date-fns'

export default function Date({ dateString }) {
  // const dateTime = new Date(dateString).toString()
  const date = parseISO(dateString)
  return <time dateTime={dateString}>{format(date, 'do LLLL yyyy')}</time>
}
