// COMMON FUNCTIONS

var endpoint = "https://live.arranmorechallenge.com";
// var endpoint = "http://localhost:2017";

loadGroups()
loadGames()

// GET RESULTS FROM ADMIN ENDPOINT
function loadGroups(){
	$.get( endpoint+"/fixtures/groups/2017", function( data ) {
		// console.log("initial load");
		console.log(data);

		if(data.groups.length == 0 ){
			$('#groupTable').html('<div class="align-center">There are no groups set up.</div>');
		} else {
			// BUILD TABLES
			console.log("these are groups");
			buildGroupTables(data)
			// HIDE THE LATEST MESSAGE ID
			// $('#groupTable').html("");
		}
	});
}

function loadGames(){
	$.get( endpoint+"/fixtures/games/2017", function( data ) {
		// console.log("initial load");
		console.log(data);

		if(data.games.length == 0 ){
			$('#groupsGamesTable').html('<div class="align-center">There are no groups set up.</div>');
		} else {
			// BUILD TABLES
			console.log("these are groups");
			buildGroupGamesTables(data);
			buildQuartersGamesTables(data);
			buildSemisGamesTables(data);
			buildFinalGamesTables(data);
			// HIDE THE LATEST MESSAGE ID
			// $('#groupTable').html("");
		}
	});
}

// var group = ["Day 1, 11:30", "Day 1, 11:55", "Day 1, 12:20", "Day 1, 12:45", "Day 1, 13:10", "Day 1, 13:35", "Day 1, 14:00", "Day 1, 14:25", "Day 1, 14:50", "Day 1, 15:15", "Day 1, 15:40", "Day 1, 16:05", "Day 1, 16:30", "Day 1, 16:55", "Day 1, 17:20", "Day 1, 17:45", "Day 2, 11:30", "Day 2, 11:55", "Day 2, 12:20", "Day 2, 12:45", "Day 2, 13:10", "Day 2, 13:35", "Day 2, 14:00", "Day 2, 14:25"];


function buildGroupGamesTables(data){
	var html = '<table class="game"><thead><tr><th colspan="5">Group Games</th></tr></thead><tbody>';
	var gg1,gg2,gg3,gg4,gg5,gg6,gg7,gg8,gg9,gg10,gg11,gg12,gg13,gg14,gg15,gg16,gg17,gg18,gg19,gg20,gg21 = "";
	for (var i = data.games.length - 1; i >= 0; i--) {
		switch (data.games[i].gameTimeID) {
			// case "Day 1, 11:30":
			// 	gg1 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 11:55":
			// 	gg2 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 12:20":
			// 	gg3 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 12:45":
			// 	gg4 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 13:10":
			// 	gg5 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 13:35":
			// 	gg6 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 14:00":
			// 	gg7 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 14:30":
			// 	gg8 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 14:55":
			// 	gg9 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 15:20":
			// 	gg10 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 15:45":
			// 	gg11 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 16:10":
			// 	gg12 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 16:35":
			// 	gg13 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 1, 17:00":
			// 	gg14 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 2, 12:00":
			// 	gg15 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 2, 12:25":
			// 	gg16 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 2, 12:50":
			// 	gg17 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 2, 13:15":
			// 	gg18 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 2, 13:50":
			// 	gg19 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 2, 14:15":
			// 	gg20 = getGameRowHTML(data.games[i])
			// 	break
			// case "Day 2, 14:40":
			// 	gg21 = getGameRowHTML(data.games[i])
			// 	break
			// NEWS
			case "Day 1, 11:30":
				gg1 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 11:55":
				gg2 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 12:20":
				gg3 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 12:45":
				gg4 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 13:10":
				gg5 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 13:35":
				gg6 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 14:00":
				gg7 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 14:25":
				gg8 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 14:50":
				gg9 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 15:15":
				gg10 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 15:40":
				gg11 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 16:05":
				gg12 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 16:30":
				gg13 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 16:55":
				gg14 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 17:20":
				gg14 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 17:45":
				gg14 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 11:30":
				gg15 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 11:55":
				gg15 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 12:20":
				gg16 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 12:45":
				gg17 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 13:10":
				gg18 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 13:35":
				gg19 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 14:00":
				gg20 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 14:25":
				gg21 = getGameRowHTML(data.games[i])
				break
		}
	}

	checkArray = [gg1,gg2,gg3,gg4,gg5,gg6,gg7,gg8,gg9,gg10,gg11,gg12,gg13,gg14,gg15,gg16,gg17,gg18,gg19,gg20,gg21];

	for (var i = checkArray.length - 1; i >= 0; i--) {
		if (checkArray[i] == undefined){
			checkArray[i] = "";
		}
	}

	html += gg1+gg2+gg3+gg4+gg5+gg6+gg7+gg8+gg9+gg10+gg11+gg12+gg13+gg14+gg15+gg16+gg17+gg18+gg19+gg20+gg21;
	html += '</tbody></table>';
	$('#groupsGamesTable').html(html);
}

function buildQuartersGamesTables(data){
	var html = '<table class="game"><thead><tr><th colspan="5">Group Games</th></tr></thead><tbody>';
	for (var i = data.games.length - 1; i >= 0; i--) {
		// console.log(data.games[i].gameTimeID);
		switch (data.games[i].gameTimeID){
			case "Quarter 1":
				qg1 = getGameRowHTML(data.games[i]);
				break
			case "Quarter 2":
				qg2 = getGameRowHTML(data.games[i]);
				break
				console.log(qg2);
			case "Quarter 3":
				qg3 = getGameRowHTML(data.games[i]);
				break
			case "Quarter 4":
				qg4 = getGameRowHTML(data.games[i]);
				break
		}
	}

	checkArray = [qg1,qg2,qg3,qg4];

	// for (var i = checkArray.length - 1; i >= 0; i--) {
	// 	if (checkArray[i] == undefined){
	// 		checkArray[i] = "";
	// 	}
	// }

	html += qg1+qg2+qg3+qg4;
	html += '</tbody></table>';

	$('#quartersGamesTable').html(html);
}

function buildSemisGamesTables(data){
	var html = '<table class="game"><thead><tr><th colspan="5">Group Games</th></tr></thead><tbody>';
	for (var i = data.games.length - 1; i >= 0; i--) {
		switch (data.games[i].gameTimeID){
			case "Semi 1":
				sg1 = getGameRowHTML(data.games[i]);
				break
			case "Semi 2":
				sg2 = getGameRowHTML(data.games[i]);
				break
		}
	}

	checkArray = [sg1,sg2];

	for (var i = checkArray.length - 1; i >= 0; i--) {
		if (checkArray[i] == undefined){
			checkArray[i] = "";
		}
	}

	html += sg1+sg2;
	html += '</tbody></table>';
	
	$('#semisGamesTable').html(html);
}

function buildFinalGamesTables(data){
	var html = '<table class="game"><thead><tr><th colspan="5">Group Games</th></tr></thead><tbody>';
	for (var i = data.games.length - 1; i >= 0; i--) {
		switch (data.games[i].gameTimeID){
			case "FINAL":
				final = getGameRowHTML(data.games[i]);
				break
		}
	}

	checkArray = [sg1,sg2];

	for (var i = checkArray.length - 1; i >= 0; i--) {
		if (checkArray[i] == undefined){
			checkArray[i] = "";
		}
	}

	html += final;
	html += '</tbody></table>';
	
	$('#finalGamesTable').html(html);
}

function getGameRowHTML(gameObj){
	var html = "<tr><td>"+gameObj.gameTimeID+"</td>"
	html += "<td>"+prettyTeamNames(gameObj.team1ID)+"</td>"
	html += "<td>"+getPrettyScore(gameObj.team1Score)+getPrettyPenalty(gameObj.team1Penalties)+getPrettySuddenDeath(gameObj.team1SuddenDeath)+"</td>"
	html += "<td>"+getPrettyScore(gameObj.team2Score)+getPrettyPenalty(gameObj.team2Penalties)+getPrettySuddenDeath(gameObj.team2SuddenDeath)+"</td>"
	html += "<td>"+prettyTeamNames(gameObj.team2ID)+"</td>"
	html +="</tr>"
	return html
}

function getPrettyScore(goalString){
	var html = "";
	if (goalString == ""){
		html += "-";
	} else {
		html += goalString;
	}
	return html
}

function getPrettyPenalty(penaltyString){
	var html = "";
	if (penaltyString != ""){
		html += ", p."+penaltyString;
	}
	return html
}

function getPrettySuddenDeath(suddenDeathString){
	var html = "";
	if (suddenDeathString != ""){
		html += ", p."+suddenDeathString;
	}
	return html
}

// <table class="game"><thead><tr><th colspan="5">Group Games</th></tr></thead><tbody><tr><td>Day 1, 11:30</td><td>Real Lypis</td><td>-</td><td>-</td><td>Donegal Town PLC</td></tr><tr><td>Day 1, 11:55</td><td>M.T.F.C.</td><td>-</td><td>-</td><td>Northern Tunnelling</td></tr><tr><td>Day 1, 12:20</td><td>Midland Warriors</td><td>-</td><td>-</td><td>The Saturdays</td></tr><tr><td>Day 1, 12:45</td><td>FC Palatico</td><td>-</td><td>-</td><td>Arranmore United</td></tr><tr><td>Day 1, 13:10</td><td>Tory Celtic</td><td>-</td><td>-</td><td>Gallagher Tunnelling</td></tr><tr><td>Day 1, 13:35</td><td>Clannad Celtic</td><td>-</td><td>-</td><td>Stonecutters</td></tr><tr><td>Day 1, 14:00</td><td>The Rebels</td><td>-</td><td>-</td><td>Damo's Dream</td></tr><tr><td>Day 1, 14:25</td><td>Dav's Dazzlers</td><td>-</td><td>-</td><td>Barhale</td></tr><tr><td>Day 1, 14:50</td><td>Real Lypis</td><td>-</td><td>-</td><td>M.T.F.C.</td></tr><tr><td>Day 1, 15:15</td><td>Donegal Town PLC</td><td>-</td><td>-</td><td>Northern Tunnelling</td></tr><tr><td>Day 1, 15:40</td><td>Midland Warriors</td><td>-</td><td>-</td><td>FC Palatico</td></tr><tr><td>Day 1, 16:05</td><td>The Saturdays</td><td>-</td><td>-</td><td>Arranmore United</td></tr><tr><td>Day 1, 16:30</td><td>Tory Celtic</td><td>-</td><td>-</td><td>Clannad Celtic</td></tr><tr><td>Day 1, 16:55</td><td>Gallagher Tunnelling</td><td>-</td><td>-</td><td>Stonecutters</td></tr><tr><td>Day 1, 17:20</td><td>The Rebels</td><td>-</td><td>Dav's Dazzlers</td><td>-</td></tr><tr><td>Day 1, 17:45</td><td>Damo's Dream</td><td>-</td><td>-</td><td>Barhale</td></tr><tr><td>Day 2, 11:30</td><td>Real Lypis</td><td>-</td><td>-</td><td>Northern Tunnelling</td></tr><tr><td>Day 2, 11:55</td><td>Donegal Town PLC</td><td>-</td><td>-</td><td>M.T.F.C.</td></tr><tr><td>Day 2, 12:20</td><td>Midland Warriors</td><td>-</td><td>-</td><td>Arranmore United</td></tr><tr><td>Day 2, 12:45</td><td>The Saturdays</td><td>-</td><td>-</td><td>FC Palatico</td></tr><tr><td>Day 2, 13:10</td><td>Tory Celtic</td><td>-</td><td>-</td><td>Stonecutters</td></tr><tr><td>Day 2, 13:35</td><td>Gallagher Tunnelling</td><td>-</td><td>-</td><td>Clannad Celtic</td></tr><tr><td>Day 2, 14:00</td><td>The Rebels</td><td>-</td><td>-</td><td>Barhale</td></tr><tr><td>Day 2, 14:25</td><td>Damo's Dream</td><td>-</td><td>-</td><td>Dav's Dazzlers</td></tr></tbody></table>

$('#saveGroup').click(function(){
	// console.log("SAVE");
	var groupID = $('#groupSelect').val();
	var positionID = $('#positionSelect').val();
	var teamID = $('#teamSelect').val()

	if (groupID && positionID && teamID != "base"){
		// console.log("post");
		groupObj = {};
		groupObj.groupID = groupID;
		groupObj.positionID = positionID;
		groupObj.teamID = teamID;
		console.log(groupObj);

		$.post( endpoint+'/fixtures/group/2017/update', groupObj)
			.done(function( data ) {
				console.log(data)
				if (data == "OK"){
					alert("update saved");
				} else {
					alert("Your update didn't send");
				}
		});

	} else {
		// console.log("dont post")
	}
});

$('#saveGame').click(function(){
	// console.log("SAVE");
	var gameTimeID = $('#gameTimes').val();
	var team1ID = $('#team1GameSelect').val();
	var team1Score = $('#team1Score').val();
	var team1Penalties = $('#team1Penalties').val();
	var team1SuddenDeath = $('#team1SuddenDeath').val();

	var team2ID = $('#team2GameSelect').val();
	var team2Score = $('#team2Score').val();
	var team2Penalties = $('#team2Penalties').val();
	var team2SuddenDeath = $('#team2SuddenDeath').val();

	if (gameTimeID != "base"){
		// console.log("post");
		gameObj = {};
		gameObj.gameTimeID = gameTimeID;
		gameObj.team1ID = team1ID;
		gameObj.team1Score = team1Score;
		gameObj.team1Penalties = team1Penalties;
		gameObj.team1SuddenDeath = team1SuddenDeath;

		gameObj.team2ID = team2ID;
		gameObj.team2Score = team2Score;
		gameObj.team2Penalties = team2Penalties;
		gameObj.team2SuddenDeath = team2SuddenDeath;
		
		console.log(gameObj);

		$.post( endpoint+'/fixtures/games/2017/update', gameObj)
			.done(function( data ) {
				console.log(data)
				if (data == "OK"){
					alert("game saved");
				} else {
					alert("Your game didn't save");
				}
		});

	} else {
		// console.log("dont post")
	}
});


var group = ["Day 1, 11:30", "Day 1, 11:55", "Day 1, 12:20", "Day 1, 12:45", "Day 1, 13:10", "Day 1, 13:35", "Day 1, 14:00", "Day 1, 14:25", "Day 1, 14:50", "Day 1, 15:15", "Day 1, 15:40", "Day 1, 16:05", "Day 1, 16:30", "Day 1, 16:55", "Day 1, 17:20", "Day 1, 17:45", "Day 2, 11:30", "Day 2, 11:55", "Day 2, 12:20", "Day 2, 12:45", "Day 2, 13:10", "Day 2, 13:35", "Day 2, 14:00", "Day 2, 14:25"];

// var group = ["Day 1, 11:30","Day 1, 11:55","Day 1, 12:20","Day 1, 12:45","Day 1, 13:10","Day 1, 13:35","Day 1, 14:00","Day 1, 14:30","Day 1, 14:55","Day 1, 15:20","Day 1, 15:45","Day 1, 16:10","Day 1, 16:35","Day 1, 17:00", "Day 2, 12:00","Day 2, 12:25","Day 2, 12:50","Day 2, 13:15","Day 2, 13:50","Day 2, 14:15","Day 2, 14:40"]

var quarter = ["Quarter 1", "Quarter 2", "Quarter 3", "Quarter 4"];

var semi = ["Semi 1", "Semi 2"];

var final = ["FINAL"];

$("#gameTypeSelect").change(function() {
    var parent = $(this).val(); 
    switch(parent){ 
        case 'group':
             list(group);
            break;
        case 'quarter':
             list(quarter);
            break;              
        case 'semi':
             list(semi);
            break;                
        case 'final':
             list(final);
            break;  
        default: //default child option is blank
            $("#gameTimes").append("<option>Select a game group</option>");
            break;
	}
});

function list(array_list){
    $("#gameTimes").html(""); //reset child options
    $(array_list).each(function (i) { //populate child options 
        $("#gameTimes").append("<option value='"+array_list[i]+"' id='"+array_list[i]+"'>"+array_list[i]+"</option>");
    });
}

function buildGroupTables(data){
	var html = '<table class="group"> <thead><tr><th>groupA</th><th>groupB</th><th>groupC</th><th>groupD</th></tr></thead><tbody>';
	for (var i = data.groups.length - 1; i >= 0; i--) {

		switch (data.groups[i].groupID) {
			case "A":
				switch (data.groups[i].positionID){
					case "1":
						var a1 = "<tr><td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "2":
						var a2 = "<tr><td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
					case "3":
						var a3 = "<tr><td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "4":
						var a4 = "<tr><td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
				}
				break
			case "B":
				switch (data.groups[i].positionID){
					case "1":
						var b1 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "2":
						var b2 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
					case "3":
						var b3 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "4":
						var b4 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
				}
				break
			case "C":
				switch (data.groups[i].positionID){
					case "1":
						var c1 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "2":
						var c2 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
					case "3":
						var c3 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "4":
						var c4 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
				}
				break
			case "D":
				switch (data.groups[i].positionID){
					case "1":
						var d1 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td></tr>"
						break
					case "2":
						var d2 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td></tr>"
						break	
					case "3":
						var d3 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td></tr>"
						break
					case "4":
						var d4 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td></tr>"
						break	
				}
				break
		}
	}

	row1 = a1+b1+c1+d1;
	row2 = a2+b2+c2+d2;
	row3 = a3+b3+c3+d3;
	row4 = a4+b4+c4+d4;

	html += row1+row2+row3+row4

	html += '</tbody></table>'

	// return html

	$('#groupTable').html(html);

}

function prettyTeamNames(teamID){
	switch (teamID) {
		case "real_lypis":
			return "Real Lypis";
			break
		case "dungloe_town_plc":
			return "Dungloe Town PLC";
			break
		case "northern_tunnelling":
			return "Northern Tunnelling";
			break
		case "midland_warriors":
			return "Midland Warriors";
			break
		case "the_saturdays":
			return "The Saturdays";
			break
		case "fc_palatico":
			return "FC Palatico";
			break
		case "arranmore_utd":
			return "Arranmore Utd";
			break
		case "tory_celtic":
			return "Tory Celtic";
			break
		case "gallagher_tunnelling":
			return "Gallagher Tunnelling";
			break
		case "clannad_celtic":
			return "Clannad Celtic";
			break
		case "stonecutters":
			return "Stonecutters";
			break
		case "the_rebels":
			return "The Rebels";
			break
		case "damos_dream":
			return "Damo's Dream";
			break
		case "davs_dazzlers":
			return "Dav's Dazzlers";
			break
		case "barhale":
			return "Barhale";
			break
		case "fc_shaktar":
			return "FC Shaktar";
			break
		case "bayern_neverlusen":
			return "Bayern Neverlusen";
			break
		case "luton_irish":
			return "Luton Irish";
			break
		case "fc_internationale":
			return "FC Internationale";
			break
		default:
			return "-";
			break
	}
}

// POST TO ADMIN ENDPOINT

function getPrettyTimeStamp(epoch){
	var date = new Date(parseInt(epoch));

	var hours = lessThanTen(date.getHours());
	var minutes = lessThanTen(date.getMinutes());

	var stringDate = lessThanTen(date.getDate());
	var stringMonth = lessThanTen(date.getMonth()+1);

	var string = hours+":"+minutes+" "+stringDate+"/"+stringMonth;
	return string;

}

function lessThanTen(number){
	if (number < 10){
		return "0"+number
	} else {
		return number
	}
}


