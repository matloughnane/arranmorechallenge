import Head from 'next/head'
import Link from 'next/link'
// import styles from '../styles/Home.module.css'

import React from 'react';

// import { getSortedPostsData } from '../lib/posts'
import {
	Stack,
	SimpleGrid,
	HStack,
	Container,
	VStack,
	Heading,
	Flex,
	Box,
	Icon,
	Text,
	Button,
	useColorModeValue, useBreakpointValue
} from '@chakra-ui/react'

import MetaHead from '../components/common/metaHead';
import NavBar from '../components/common/navbar';
import Footer from '../components/common/footer';
import { InfoIcon } from '@chakra-ui/icons';
import ImageDivider from '../components/common/imageDivider';

const features = [
	{ "text": "Minimum of 7, maximum 11 players must be registered prior to the first game." },
	{ "text": "All competing teams must start each fixture with 7 players however an amendment to the existing rule is as follows:" },
	{ "text": "All teams are permitted to have 1 player miss the first game but this same player must play in the second game to avoid being banned from the competition." },
	{ "text": "Teams are only allowed 11 named players. These must be registered prior to a team's 1st game. Cost of registration for each team €250." },
	{ "text": "10 minutes per half with Roll on Roll off substitutes." },
	{ "text": "The top 2 teams from each group go into the knockout stages." },
];

const qualifying = [
	{ "text": "In the event of 2 teams being even at the end of the group stage the following will determine which team progresses. \nFirstly- goal difference. Then goals scored. Then record against each other. If this fails to separate the teams it will be decided on 3 penalties each" },
	{ "text": "Knockout Stage Matches: 10 minutes a side, in the event of a draw: 3 penalties each. Then sudden death penalties." },
	{ "text": "Final Matches: 15 minutes a side, in the event of a draw: 3 penalties each. Then sudden death penalties." },
];

export default function About() {
	return (
		<>

			<MetaHead title={"About Arranmore Island's Football Competition"} />

			<NavBar />
			<Box p={0}>
				<Stack spacing={4} as={Container} mb={10} maxW={'3xl'} textAlign={'left'}>
					<Heading mt={10} size={'xl'}>About The Challenge</Heading>
					<Heading size={'md'}>Note from the Arranmore Committee</Heading>
					<Text color={useColorModeValue('gray.600', 'gray.400')} fontSize={'md'}>
						The committee would like to remind teams and fans alike that there should be no rubbish left pitchside at the Rannagh. There is no caretaker anymore and in years past there has always an issue with the amount of rubbish that is not put in bins. We cannot stress enough that there are plenty of bins around the field if everyone could dispose of their rubbish, it would greatly be of help to all of us.
						<br /> Thanks very much and enjoy the weekend!
       			 </Text>
				</Stack>


				<ImageDivider image={"/images/backgrounds/pitch.jpg"} headline={"Arranmore Challenge is managed by Arranmore United at the Rannagh Football Pitch."} />

				<Stack spacing={4} mt={5} as={Container} maxW={'4xl'} textAlign={'center'}>
					<Heading my="5" fontSize={'3xl'}>Arranmore Challenge Rules</Heading>
					<Text color={useColorModeValue('gray.600', 'gray.400')} fontSize={'xl'}>
					</Text>
				</Stack>

				<Container maxW={'5xl'} mb={10}>
					<SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing={10}>
						{features.map((feature) => (
							<HStack key={feature.id} align={'top'}>
								<Box color={useColorModeValue('gray.400', 'gray.50')} px={2}>
									<Icon as={InfoIcon} />
								</Box>
								<VStack align={'start'}>
									{/* <Text fontWeight={600}>{feature.title}</Text> */}
									<Text color={useColorModeValue('gray.600', 'gray.400')}>{feature.text}</Text>
								</VStack>
							</HStack>
						))}
					</SimpleGrid>
				</Container>
				<Stack spacing={4} as={Container} maxW={'4xl'} textAlign={'center'}>
					<Heading my="5" fontSize={'3xl'}>Qualifying Rules</Heading>
					<Text color={useColorModeValue('gray.600', 'gray.400')} fontSize={'xl'}>
					</Text>
				</Stack>

				<Container maxW={'5xl'} mb={10}>
					<SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing={10}>
						{qualifying.map((feature) => (
							<HStack key={feature.id} align={'top'}>
								<Box color={useColorModeValue('gray.400', 'gray.50')} px={2}>
									<Icon as={InfoIcon} />
								</Box>
								<VStack align={'start'}>
									{/* <Text fontWeight={600}>{feature.title}</Text> */}
									<Text color={useColorModeValue('gray.600', 'gray.400')}>{feature.text}</Text>
								</VStack>
							</HStack>
						))}
					</SimpleGrid>
				</Container>

				<ImageDivider image={"/images/backgrounds/aphort.jpg"} headline={"Arranmore Challenge is managed by Arranmore United at the Rannagh Football Pitch."} />

				<Stack spacing={4} as={Container} my={10} maxW={'3xl'} textAlign={'left'}>
					<Heading size={'lg'}>The Fine Print</Heading>
					<Heading size={'sm'}>Bookings:</Heading>
					<Text color={useColorModeValue('gray.600', 'gray.400')} fontSize={'md'}>
						If a player receives a red card he will be banned from the next match.
						If a player receives 2 yellow cards he will be banned from the next match.
       			 </Text>
					<Heading size={'sm'}>Late Arrivals:</Heading>
					<Text color={useColorModeValue('gray.600', 'gray.400')} fontSize={'md'}>
						In the event of teams arriving late for matches the following sanctions will be imposed.
						5 minutes late: - The late team will be 1 - 0 down.
						10 minutes late: - The late team will be 2 - 0 down.
						15 minutes late: - LOSE. 3-0.
						Any team that fails to fulfill its 3 fixtures will be suspended from next 3 years challenge.
       			 </Text>
					<Heading size={'sm'}>All present teams will remain until they:</Heading>
					<Text color={useColorModeValue('gray.600', 'gray.400')} fontSize={'md'}>
						1. No Longer wish to take part or
						2. The committee expel them for a serious breach of the Rules or general conduct either on or of the field of play. The Club due to insurance & safety reasons will not allow the consumption of alcohol inside the perimeter of the field at any time during the challenge and includes all personnel. Anyone found to be in breach of this rule will be asked to stay outside the perimeter which is the fenced area around the field, we hope and expect these rules to be adhered to.
						ALL TEAMS THROUGH TO THE QUARTER FINALS MUST BE PRESENT FOR THE DRAW AT RANNAGH ON SUNDAY AT 2:50 PM
       			 </Text>
				</Stack>

			</Box>

			<Footer />
		</>
	)
}
