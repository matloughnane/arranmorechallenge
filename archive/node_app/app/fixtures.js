
const mongodb = require('mongodb');
const MongoClient = require('mongodb').MongoClient;

// app/functions.js

module.exports = function(app, passport) {


    // ========  RETRIEVAL
    // ================================

    app.get('/fixtures/groups/2017' , function(req, res) {
        var reqid = req.params.id;
        // console.log(reqid);

        // var filter = {'year': 2017 };

        db.collection('groups').find().toArray( function (err, result) {
            if (err) return res.send(err);
            // res.send(result)
            // res.send(200);
            // console.log(result[0])
            // res.render('profile.ejs', {
            //     user : result[0] 
            // });
            res.json({ "groups": result });
        })

    });


    app.get('/fixtures/games/2017' , function(req, res) {
        var reqid = req.params.id;
        // console.log(reqid);

        // var filter = {'year': 2017 };

        db.collection('games').find().toArray( function (err, result) {
            if (err) return res.send(err);
            // res.send(result)
            // res.send(200);
            // console.log(result[0])
            // res.render('profile.ejs', {
            //     user : result[0] 
            // });
            res.json({ "games": result });
        })

    });

    // ========  ADMIN FUNCTIONS
    // ================================



    // ========  INSERTION
    // ================================

    app.post('/fixtures/group/2017/update' , function(req, res) {
        var reqBody = req.body;
        // console.log(reqid);
        var filter = {"groupID": reqBody.groupID, "positionID": reqBody.positionID }
        db.collection('groups').findOneAndUpdate(filter, {$set: {"teamID": reqBody.teamID}}, {upsert:true}, function(err, result){
            if(err){
                return res.send(404);
            };
            res.send(200);
        });

    });


    app.post('/fixtures/games/2017/update' , function(req, res) {
        var reqBody = req.body;
        // console.log(reqid);
        var filter = {"gameTimeID": reqBody.gameTimeID }
        db.collection('games').findOneAndUpdate(filter, {$set: {"gameTimeID": reqBody.gameTimeID,"team1ID": reqBody.team1ID,"team1Score": reqBody.team1Score,"team1Penalties": reqBody.team1Penalties,"team1SuddenDeath": reqBody.team1SuddenDeath,"team2ID": reqBody.team2ID,"team2Score": reqBody.team2Score,"team2Penalties": reqBody.team2Penalties,"team2SuddenDeath": reqBody.team2SuddenDeath}}, {upsert:true}, function(err, result){
            if(err){
                return res.send(404);
            };
            res.send(200);
        });

    });




};

