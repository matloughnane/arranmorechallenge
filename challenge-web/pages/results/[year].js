// import Head from 'next/head'
import Link from 'next/link'
// import styles from '../styles/Home.module.css'

import React from 'react';

// import { getSortedPostsData } from '../lib/posts'
import {
	Stack,
	Container,
	Heading,
	Box,
	Table,
	// Thead,
	Tbody,
	// Tfoot,
	// Button,
	// Badge,
	Tr,
	// Th,
	Td,
	TableCaption,
	Text,
	Divider,
	// Wrap,
	// WrapItem,
	// useColorModeValue
} from '@chakra-ui/react'
import NavBar from '../../components/common/navbar';
import Footer from '../../components/common/footer';

// import { getBadgeColour } from '../../lib/utils';

// import { InfoIcon } from '@chakra-ui/icons';

import { getArchivedYears, getYearParams, getHistoricalWinners, getYearData } from '../../lib/results'
import Date from '../../components/date';
import CompetitionTable from '../../components/tables/competitionTable'
import FlatTable from '../../components/tables/flatTable'
import MetaHead from '../../components/common/metaHead';

export async function getStaticPaths() {
	const paths = getYearParams();
	return {
		paths,
		fallback: false
	}
}


export function getStaticProps({ params }) {
	// const historicalWinners = getHistoricalWinners();
	// const archivedYears = getArchivedYears();
	// console.log("archivedYears");
	// const year = params.year
	// console.log(year);
	const yearData = getYearData(params.year)
	// console.log(yearData);

	return {
		props: {
			yearData,
			// historicalWinners,
			// archivedYears
		}
	}
}

export default function Year({ yearData }) {
	return (
		<>
		<MetaHead title={`${yearData.year} Arranmore Challenge Results`} />
			<NavBar />
			<Box p={0}>
				<Stack spacing={4} as={Container} mb={10} maxW={'4xl'} textAlign={'left'}>
					<Heading mt={10} size={'xl'}>{yearData.year} Arranmore Challenge</Heading>
					<Text>The results from the {yearData.year} Arranmore Challenge.</Text>
					{yearData.groups ? <><Heading mt={10} size={'lg'}>{yearData.year} Groups</Heading>
						<Divider />
						<Table variant="striped">
							<TableCaption>Groups from the {yearData.year} Arranmore Challenge</TableCaption>
							<Tbody>
								{yearData.groups.map((e, index) => {
									// console.log(e);
									return <Tr>
										<Td><strong>{e.group}</strong></Td>
										<Td>{e["1"]}</Td>
										<Td>{e["2"]}</Td>
										<Td>{e["3"]}</Td>
										<Td>{e["4"]}</Td>
									</Tr>
								}
								)}
							</Tbody>
						</Table></> : <></>}
					<Divider />
					<Heading mt={10} size={'lg'}>{yearData.year} Results</Heading>

					{yearData.type ? <CompetitionTable results={yearData.results} year={yearData.year} /> : <FlatTable results={yearData.results} year={yearData.year} />}

					<Divider />
					{/* <Heading mt={10} size={'md'}>Results from Past Challenges</Heading> */}
					{/* <Text>{JSON.stringify(archivedYears)}</Text> */}
					{/* <Text>{JSON.stringify(yearData.results)}</Text> */}
				</Stack>

				{/* <Stack spacing={4} as={Container} mb={10} maxW={'4xl'} textAlign={'left'}>
					<Heading mt={10} size={'lg'}>History of Champions</Heading>

				</Stack> */}
			</Box>
			<Footer />
		</>
	)
}
