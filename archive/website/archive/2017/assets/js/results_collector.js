// // FIREBASE REFERENCES
// // var firebaseRef = new Firebase("https://amber-fire-55.firebaseio.com/arranmore_challenge/");
// var config = {
// apiKey: "AIzaSyDmPNEL0kDxdK4S-4vMRxfJcA8alBm8W_k",
// authDomain: "arranmorefastferry-1311.firebaseapp.com",
// databaseURL: "https://arranmorefastferry-1311.firebaseio.com",
// storageBucket: "arranmorefastferry-1311.appspot.com",
// };
// var firebaseNew = firebase.initializeApp(config);
// var firebaseRef = firebaseNew.database().ref("/arranmore_challenge/");


// var resultsObj = {};

// firebaseRef.on("value", function(snapshot) {
//     resultsObj = snapshot.val();
//     // GROUPS TABLE
//     displayGroups(resultsObj);
//     // GROUPS GAMES
//     displayGroupGames(resultsObj);
//     displayQuarterGames(resultsObj);
//     displaySemiGames(resultsObj);
//     displayFinalGame(resultsObj);
// });

// function displayGroups(object){
// 	var groupObj = object.games.groups;
// 	// console.log(groupObj);
// 	var groupNames = [];
// 	var team1Array = [];
// 	var team2Array = [];
// 	var team3Array = [];
// 	var team4Array = [];
// 	// GROUP ARRAY
// 	for (key in groupObj){
// 		groupNames.push(key);
// 	}
// 	// TEAM 1 ARRAY
// 	for (key in groupObj){
// 		team1Array.push(groupObj[key].Team1);
// 	}
// 	// 	console.log(team1Array);
// 	// TEAM 2 ARRAY
// 	for (key in groupObj){
// 		team2Array.push(groupObj[key].Team2);
// 	}
// 	// console.log(team2Array);
// 	// TEAM 3 ARRAY
// 	for (key in groupObj){
// 		team3Array.push(groupObj[key].Team3);
// 	}
// 	// console.log(team3Array);
// 	// TEAM 4 ARRAY
// 	for (key in groupObj){
// 		team4Array.push(groupObj[key].Team4);
// 	}
// 	// console.log(team4Array);
// 	var teamArrays = [team1Array,team2Array,team3Array,team4Array];
// 	// console.log(teamArrays);
// 	createGroupTable(groupNames, teamArrays);
// };
// function createGroupTable(groupName, teamArrays){
// 	var result = "<table  class='group'> <thead>";
// 	for (var i = 0; i < groupName.length; i++) {
// 		result += "<th>" + groupName[i] + "</th>";
// 	};
// 	result += "</thead>";
// 	// console.log(result);
// 	for (var i = 0; i < teamArrays.length; i++) {
// 		result += "<tr>";
// 		var teamArraySingle = teamArrays[i];
// 		for (var j = 0; j < teamArraySingle.length; j++) {
// 			// console.log(teamArraySingle[j]);
// 			result += "<td>"+teamArraySingle[j]+"</td>";
// 		};
// 		result += "<tr>";
// 	};
// 	result += "</table>";
// 	document.getElementById("group_table").innerHTML = result;
// };

// function displayGroupGames(object){
// 	var gamesObj = object.games.group_games;
// 	// console.log(gamesObj);
// 	buildTable(gamesObj, "Group Games", "group_games_table")
// };
// function displayQuarterGames(object){
// 	var gamesObj = object.games.quarter_games;
// 	// console.log(gamesObj);
// 	buildTable(gamesObj, "Quarter Finals", "quarters_table")
// };
// function displaySemiGames(object){
// 	var gamesObj = object.games.semi_games;
// 	// console.log(gamesObj);
// 	buildTable(gamesObj, "Semi Finals", "semis_table")
// };
// function displayFinalGame(object){
// 	var gamesObj = object.games.final_game;
// 	// console.log(gamesObj);
// 	buildTable(gamesObj, "THE FINAL", "final_table")
// };

// function buildTable(object, title, id){
// 	var result = "<table class='game'><thead><th colspan='5'>"+title+"</th></thead>";
// 	for (key in object){
// 		result += "<tr>";
// 		// console.log(object[key]);
// 		result += "<td>"+object[key].time+"</td>";
// 		result += "<td>"+object[key].team1+"</td>";
// 		result += "<td>"+object[key].team1_score+"</td>";
// 		result += "<td>"+object[key].team2_score+"</td>";
// 		result += "<td>"+object[key].team2+"</td>";
// 		result += "</tr>";
// 	}
// 	result += "</table>";
// 	document.getElementById(""+id+"").innerHTML = result;
// };



// var endpoint = "http://localhost:2017";
var endpoint = "https://live.arranmorechallenge.com";

loadGroups()
loadGames()

// GET RESULTS FROM ADMIN ENDPOINT
function loadGroups(){
	$.get( endpoint+"/fixtures/groups/2017", function( data ) {
		// console.log("initial load");
		console.log(data);

		if(data.groups.length == 0 ){
			$('#groupTable').html('<div class="align-center">There are no groups set up.</div>');
		} else {
			// BUILD TABLES
			console.log("these are groups");
			buildGroupTables(data)
			// HIDE THE LATEST MESSAGE ID
			// $('#groupTable').html("");
		}
	});
}

function loadGames(){
	$.get( endpoint+"/fixtures/games/2017", function( data ) {
		// console.log("initial load");
		console.log(data);

		if(data.games.length == 0 ){
			$('#groupsGamesTable').html('<div class="align-center">There are no groups set up.</div>');
		} else {
			// BUILD TABLES
			console.log("these are groups");
			buildGroupGamesTables(data);
			buildQuartersGamesTables(data);
			buildSemisGamesTables(data);
			buildFinalGamesTables(data);
			// HIDE THE LATEST MESSAGE ID
			// $('#groupTable').html("");
		}
	});
}



function buildGroupGamesTables(data){
	var html = '<table class="game"><thead><tr><th colspan="5">Group Games</th></tr></thead><tbody>';
	for (var i = data.games.length - 1; i >= 0; i--) {
		switch (data.games[i].gameTimeID) {
			case "Day 1, 11:30":
				gg1 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 11:55":
				gg2 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 12:20":
				gg3 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 12:45":
				gg4 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 13:10":
				gg5 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 13:35":
				gg6 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 14:00":
				gg7 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 14:30":
				gg8 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 14:55":
				gg9 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 15:20":
				gg10 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 15:45":
				gg11 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 16:10":
				gg12 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 16:35":
				gg13 = getGameRowHTML(data.games[i])
				break
			case "Day 1, 17:00":
				gg14 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 12:00":
				gg15 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 12:25":
				gg16 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 12:50":
				gg17 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 13:15":
				gg18 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 13:50":
				gg19 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 14:15":
				gg20 = getGameRowHTML(data.games[i])
				break
			case "Day 2, 14:40":
				gg21 = getGameRowHTML(data.games[i])
				break
		}
	}

	checkArray = [gg1,gg2,gg3,gg4,gg5,gg6,gg7,gg8,gg9,gg10,gg11,gg12,gg13,gg14,gg15,gg16,gg17,gg18,gg19,gg20,gg21];

	for (var i = checkArray.length - 1; i >= 0; i--) {
		if (checkArray[i] == undefined){
			checkArray[i] = "";
		}
	}

	html += gg1+gg2+gg3+gg4+gg5+gg6+gg7+gg8+gg9+gg10+gg11+gg12+gg13+gg14+gg15+gg16+gg17+gg18+gg19+gg20+gg21;
	html += '</tbody></table>';
	$('#groupsGamesTable').html(html);
}

function buildQuartersGamesTables(data){
	var html = '<table class="game"><thead><tr><th colspan="5">Quarter Finals</th></tr></thead><tbody>';
	for (var i = data.games.length - 1; i >= 0; i--) {
		// console.log(data.games[i].gameTimeID);
		switch (data.games[i].gameTimeID){
			case "Quarter 1":
				qg1 = getGameRowHTML(data.games[i]);
				break
			case "Quarter 2":
				qg2 = getGameRowHTML(data.games[i]);
				break
				console.log(qg2);
			case "Quarter 3":
				qg3 = getGameRowHTML(data.games[i]);
				break
			case "Quarter 4":
				qg4 = getGameRowHTML(data.games[i]);
				break
		}
	}

	checkArray = [qg1,qg2,qg3,qg4];

	// for (var i = checkArray.length - 1; i >= 0; i--) {
	// 	if (checkArray[i] == undefined){
	// 		checkArray[i] = "";
	// 	}
	// }

	html += qg1+qg2+qg3+qg4;
	html += '</tbody></table>';

	$('#quartersGamesTable').html(html);
}

function buildSemisGamesTables(data){
	var html = '<table class="game"><thead><tr><th colspan="5">Semi Finals</th></tr></thead><tbody>';
	for (var i = data.games.length - 1; i >= 0; i--) {
		switch (data.games[i].gameTimeID){
			case "Semi 1":
				sg1 = getGameRowHTML(data.games[i]);
				break
			case "Semi 2":
				sg2 = getGameRowHTML(data.games[i]);
				break
		}
	}

	checkArray = [sg1,sg2];

	for (var i = checkArray.length - 1; i >= 0; i--) {
		if (checkArray[i] == undefined){
			checkArray[i] = "";
		}
	}

	html += sg1+sg2;
	html += '</tbody></table>';
	
	$('#semisGamesTable').html(html);
}

function buildFinalGamesTables(data){
	var html = '<table class="game"><thead><tr><th colspan="5">The Final</th></tr></thead><tbody>';
	for (var i = data.games.length - 1; i >= 0; i--) {
		switch (data.games[i].gameTimeID){
			case "FINAL":
				final = getGameRowHTML(data.games[i]);
				break
		}
	}

	checkArray = [sg1,sg2];

	for (var i = checkArray.length - 1; i >= 0; i--) {
		if (checkArray[i] == undefined){
			checkArray[i] = "";
		}
	}

	html += final;
	html += '</tbody></table>';
	
	$('#finalGamesTable').html(html);
}

function getGameRowHTML(gameObj){
	var html = "<tr><td>"+gameObj.gameTimeID+"</td>"
	html += "<td>"+prettyTeamNames(gameObj.team1ID)+"</td>"
	html += "<td>"+getPrettyScore(gameObj.team1Score)+getPrettyPenalty(gameObj.team1Penalties)+getPrettySuddenDeath(gameObj.team1SuddenDeath)+"</td>"
	html += "<td>"+getPrettyScore(gameObj.team2Score)+getPrettyPenalty(gameObj.team2Penalties)+getPrettySuddenDeath(gameObj.team2SuddenDeath)+"</td>"
	html += "<td>"+prettyTeamNames(gameObj.team2ID)+"</td>"
	html +="</tr>"
	return html
}

function getPrettyScore(goalString){
	var html = "";
	if (goalString == ""){
		html += "-";
	} else {
		html += goalString
	}
	return html
}

function getPrettyPenalty(penaltyString){
	var html = "";
	if (penaltyString != ""){
		html += ", p."+penaltyString;
	}
	return html
}

function getPrettySuddenDeath(suddenDeathString){
	var html = "";
	if (suddenDeathString != ""){
		html += ", p."+suddenDeathString;
	}
	return html
}


var group = ["Day 1, 11:30", "Day 1, 11:55", "Day 1, 12:20", "Day 1, 12:45", "Day 1, 13:10", "Day 1, 13:35", "Day 1, 14:00", "Day 1, 14:25", "Day 1, 14:50", "Day 1, 15:15", "Day 1, 15:40", "Day 1, 16:05", "Day 1, 16:30", "Day 1, 16:55", "Day 1, 17:20", "Day 1, 17:45", "Day 2, 11:30", "Day 2, 11:55", "Day 2, 12:20", "Day 2, 12:45", "Day 2, 13:10", "Day 2, 13:35", "Day 2, 14:00", "Day 2, 14:25"];

var quarter = ["Quarter 1", "Quarter 2", "Quarter 3", "Quarter 4"];

var semi = ["Semi 1", "Semi 2"];

var final = ["FINAL"];

$("#gameTypeSelect").change(function() {
    var parent = $(this).val(); 
    switch(parent){ 
        case 'group':
             list(group);
            break;
        case 'quarter':
             list(quarter);
            break;              
        case 'semi':
             list(semi);
            break;                
        case 'final':
             list(final);
            break;  
        default: //default child option is blank
            $("#gameTimes").append("<option>Select a game group</option>");
            break;
	}
});

function list(array_list){
    $("#gameTimes").html(""); //reset child options
    $(array_list).each(function (i) { //populate child options 
        $("#gameTimes").append("<option value='"+array_list[i]+"' id='"+array_list[i]+"'>"+array_list[i]+"</option>");
    });
}

function buildGroupTables(data){
	var html = '<table class="group"> <thead><tr><th>groupA</th><th>groupB</th><th>groupC</th><th>groupD</th></tr></thead><tbody>';
	for (var i = data.groups.length - 1; i >= 0; i--) {

		switch (data.groups[i].groupID) {
			case "A":
				switch (data.groups[i].positionID){
					case "1":
						var a1 = "<tr><td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "2":
						var a2 = "<tr><td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
					case "3":
						var a3 = "<tr><td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "4":
						var a4 = "<tr><td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
				}
				break
			case "B":
				switch (data.groups[i].positionID){
					case "1":
						var b1 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "2":
						var b2 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
					case "3":
						var b3 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "4":
						var b4 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
				}
				break
			case "C":
				switch (data.groups[i].positionID){
					case "1":
						var c1 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "2":
						var c2 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
					case "3":
						var c3 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break
					case "4":
						var c4 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td>"
						break	
				}
				break
			case "D":
				switch (data.groups[i].positionID){
					case "1":
						var d1 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td></tr>"
						break
					case "2":
						var d2 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td></tr>"
						break	
					case "3":
						var d3 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td></tr>"
						break
					case "4":
						var d4 = "<td>"+prettyTeamNames(data.groups[i].teamID)+"</td></tr>"
						break	
				}
				break
		}
	}

	row1 = a1+b1+c1+d1;
	row2 = a2+b2+c2+d2;
	row3 = a3+b3+c3+d3;
	row4 = a4+b4+c4+d4;

	html += row1+row2+row3+row4

	html += '</tbody></table>'

	// return html

	$('#groupTable').html(html);

}

function prettyTeamNames(teamID){
	switch (teamID) {
		case "real_lypis":
			return "Real Lypis";
			break
		case "dungloe_town_plc":
			return "Dungloe Town PLC";
			break
		case "mtfc":
			return "M.T.F.C.";
			break
		case "northern_tunnelling":
			return "Northern Tunnelling";
			break
		case "midland_warriors":
			return "Midland Warriors";
			break
		case "the_saturdays":
			return "The Saturdays";
			break
		case "fc_palatico":
			return "FC Palatico";
			break
		case "arranmore_utd":
			return "Arranmore Utd";
			break
		case "tory_celtic":
			return "Tory Celtic";
			break
		case "gallagher_tunnelling":
			return "Gallagher Tunnelling";
			break
		case "clannad_celtic":
			return "Clannad Celtic";
			break
		case "stonecutters":
			return "Stonecutters";
			break
		case "the_rebels":
			return "The Rebels";
			break
		case "damos_dream":
			return "Damo's Dream";
			break
		case "davs_dazzlers":
			return "Dav's Dazzlers";
			break
		case "barhale":
			return "Barhale";
			break
		default:
			return "-";
			break
	}
}
