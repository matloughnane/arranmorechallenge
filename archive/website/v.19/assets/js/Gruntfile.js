module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		uglify: {
			options: {
				mangle: false
			},
			my_target: {
				files: {
					'common.min.js': ['jquery.js','sidepanel.js', 'jquery-lean-modal.js'],
					'index.min.js': ['common.min.js', 'countdown-check.js','countdown.js'],
					'index.min.ie.js': ['common.min.js', 'countdown-check.ie.js','countdown.js'],
				}
			}
		},
		htmlmin: {                                     // Task
			dist: {                                      // Target
				options: {                                 // Target options
					removeComments: true,
					collapseWhitespace: true
				},
				files: {                                   // Dictionary of files
					'../../dist/index.html': '../../index.html',
					'../../dist/about.html': '../../about.html',
					'../../dist/fixtures.html': '../../fixtures.html',
					'../../dist/twitter.html': '../../twitter.html',
					'../../dist/photos.html': '../../photos.html',
					// '../../dist/offline.html': '../../offline.html',
					'../../dist/404.html': '../../404.html',
					'../../dist/ie/index.html': '../../ie/index.html',
					'../../dist/ie/about.html': '../../ie/about.html',
					'../../dist/ie/fixtures.html': '../../ie/fixtures.html'
				}
			}
		},
		watch: {
			scripts: {
					files: ['*.js', '../css/partials/*.scss', '../../*.html'],
					tasks: ['uglify', 'htmlmin','sass'],
				options: {
					spawn: false,
				},
			},
		},
		sass: {
			dist: {
				options: {                       // Target options
        			style: 'compressed'
      			},
				files: {
					'../css/styles.css': '../css/styles.scss'
				}
			}
		}

	});

	// These plugins provide necessary tasks.
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task.
	grunt.registerTask('default', ['htmlmin','uglify','sass']);

};
