// import Head from 'next/head'
import Link from 'next/link'
// import styles from '../styles/Home.module.css'

import React from 'react';

// import { getSortedPostsData } from '../lib/posts'
import {
	Heading,
	Table,
	Thead,
	Tbody,
	Tfoot,
	Badge,
	Tr,
	Th,
	Td,
	TableCaption,
} from '@chakra-ui/react'

import  TeamScore from '../../components/tables/teamScore'
import { getBadgeColour } from '../../lib/utils';

export default function SingleCompTable(props) {

	// const groupResults = props.results.filter((r) => r.type === "group");

	return (
		<>
			<Heading mt={20} size={'md'}>{props.title}</Heading>
			<Table variant="striped" colorScheme="gray">
				{props.caption ? <TableCaption>Results from the {props.year} Arranmore Challenge</TableCaption> : <></>}
				<Thead>
					<Tr>
						<Th>Type</Th>
						<Th>Time</Th>
						<Th>Team</Th>
						<Th>Score</Th>
						<Th>Score</Th>
						<Th>Team</Th>
					</Tr>
				</Thead>
				<Tbody>
					{props.results.map((e) =>
						<Tr>
							{/* <Td><Date dateString={e.datetime} /></Td> */}
							<Td><Badge bg={getBadgeColour(e.type)} color={"white"}>{e.type.toUpperCase()}</Badge></Td>
							<Td>{e.datetime} </Td>
							{e.score1 + e.pen1 + e.sd1 > e.score2 + e.pen2 + e.sd2 ? <Td><strong>{e.team1}</strong></Td> : <Td>{e.team1}</Td>}
							<Td><TeamScore score={e.score1} pen={e.pen1} sd={e.sd1} /></Td>
							<Td><TeamScore score={e.score2} pen={e.pen2} sd={e.sd2} /></Td>
							{e.score1 + e.pen1 + e.sd1 < e.score2 + e.pen2 + e.sd2 ? <Td><strong>{e.team2}</strong></Td> : <Td>{e.team2}</Td>}
						</Tr>
					)}
				</Tbody>
				{props.footer ? <Tfoot>
					<Tr>
						<Th>Type</Th>
						<Th>Time</Th>
						<Th>Team</Th>
						<Th>Score</Th>
						<Th>Score</Th>
						<Th>Team</Th>
					</Tr>
				</Tfoot> : <></>}
			</Table>
		</>
	)
}
