import Head from 'next/head'
import Link from 'next/link'
// import styles from '../styles/Home.module.css'

import React from 'react';

import { getSortedPostsData } from '../lib/posts'
import { Heading, Flex, Box, Badge, Text } from '@chakra-ui/react'
import Date from '../components/date'
import NavBar from '../components/common/navbar'
import Footer from '../components/common/footer'
import MetaHead from '../components/common/metaHead';

export function getStaticProps() {
	const allPostsData = getSortedPostsData()
	return {
		props: {
			allPostsData
		}
	}
}

export default function Blog({ allPostsData }) {
	return (

		<div>
			<MetaHead title="Blog for the Arranmore Challenge" />
			<NavBar />

			<main>
				<section>

					<Box alignItems="center" maxW="900" style={{ margin: '0 auto' }} px={20}>
						<Heading py={10} size="lg">Blog</Heading>

						{allPostsData.map(({ id, date, title }) => (
							<Link href={`/posts/${id}`} key={id} >
								<Box maxW="sm" borderWidth="1px" mb="4" borderRadius="lg" overflow="hidden" style={{ cursor: "pointer" }}>
									<Box p="6">
										<Box mt="1" fontWeight="semibold" as="h4" lineHeight="7" > {title} </Box>

										<Box d="flex" alignItems="baseline">
											<Badge borderRadius="full" px="2" colorScheme="teal">
												<Date dateString={date} />
											</Badge>
										</Box>

										<Text>ID: {id}</Text>

									</Box>
								</Box>
							</Link>
						))}
					</Box>
				</section>
			</main>

			<Footer />

		</div>
	)
}
