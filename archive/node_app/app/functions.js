
const mongodb = require('mongodb');
const MongoClient = require('mongodb').MongoClient;

// app/functions.js

module.exports = function(app, passport) {


    // ========  PROFILE
    // ================================

    app.get('/profile/:id' , function(req, res) {
        var reqid = req.params.id;
        // console.log(reqid);

        var filter = {'_id': new mongodb.ObjectID(reqid)};

        db.collection('users').find(filter,{"local.password": 0, "twitter.password":0 }).toArray( function (err, result) {
            if (err) return res.send(err);
            // res.send(result)
            // res.send(200);
            // console.log(result[0])
            res.render('profile.ejs', {
                user : result[0] 
            });
        })
    });


    app.get('/profileMessages/:id' , function(req, res) {
        var reqid = req.params.id;
        // console.log(reqid);

        var filter = {"userID": reqid, "deleted": 0};

        db.collection('messages').find(filter).sort({_id: -1}).toArray( function (err, result) {
            if (err) return res.send(err);
            // res.send(result)
            // res.send(200);
            // console.log(result[0])
            // res.render('profile.ejs', {
            //     messages : result[0] 
            // });
            res.json({ 'messages': result });
        })

    });

    // ============
    // PREFERENCES ========
    // =====================================

    app.get('/user/preference-auto/:id/:pref', function(req, res) {
        var reqid = req.params.id;
        var reqpref = req.params.pref;
        // console.log(reqid);
        // console.log(reqpref);
        
        // res.send(200);
        var filter = {'_id': new mongodb.ObjectID(reqid)};

        db.collection('users').findOneAndUpdate(filter, {
            $set: {
                "refreshPreference": req.params.pref
            }
        }, function (err, result) {
            if (err) return res.send(err);
            // res.send(result)
            res.send(200);
        })

    });

    // =====================================
    // ACTIVATE ADMIN ========
    // =====================================
    // http://localhost:8080/user/preference-auto/587a1aa46800df3f1dde6b0f/true

    // http://localhost:2017/user/admin/5ce2c3a94eeb315d05000001/true

    //https://live.arranmorechallenge.com/user/admin/5ce2e22355c25cd373000001/true

    app.get('/user/admin/:id/:pref', function(req, res) {
        var reqid = req.params.id;
        var reqpref = req.params.pref;
        // console.log(reqid);
        // console.log(reqpref);
        
        // res.send(200);
        var filter = {'_id': new mongodb.ObjectID(reqid)};

        db.collection('users').findOneAndUpdate(filter, {
            $set: {
                "admin": req.params.pref
            }
        }, function (err, result) {
            if (err) return res.send(err);
            // res.send(result)
            res.send(200);
        })

    });

    // GET UPDATES
    app.get('/updates' , function(req, res) {
        var filter = {"deleted": 0}
        db.collection('messages').find(filter).sort({_id: -1}).limit(20).toArray(function(err, result){
            if(err){
                return res.send(404);
            };
            //BASIC RESULT
            res.json({ 'messages': result });
        })
    });


    // GET LATEST UPDATES
    app.get('/updates/latest/:timestamp' , function(req, res) {
        
        var reqtimestamp = parseInt(req.params.timestamp);
        console.log(reqtimestamp);

        var filter = { "timestamp": { $gt: reqtimestamp}, "deleted": 0 };

        db.collection('messages').find(filter).sort({_id: -1}).limit(20).toArray(function(err, result){
            if(err){
                return res.send(404);
            };
            //BASIC RESULT
            console.log(result);
            res.json({ 'messages': result });
        })
    });


    // GET OLDER UPDATES
    // db.messages.find({ "timestamp": { $lt: 1495882908085} }).sort({_id: -1}).limit(20)
    app.get('/updates/older/:timestamp' , function(req, res) {
        var reqtimestamp = parseInt(req.params.timestamp);

        var filter = { "timestamp": { $lt: reqtimestamp}, "deleted": 0 };

        db.collection('messages').find(filter).limit(20).toArray(function(err, result){
            if(err){
                return res.send(404);
            };
            //BASIC RESULT
            // console.log(result);
            res.json({ 'messages': result });
        })
    });

    // POST UPDATE
    app.post('/update/post', function(req,res){
        // console.log(req.body);
        message = {};
        message.userID = req.body.userID
        message.username = req.body.username
        message.adminStatus = req.body.adminStatus
        message.content = req.body.content
        message.timestamp = parseInt(req.body.timestamp)
        message.deleted = 0;
        db.collection('messages').save(message, function(err, result){
            if(err){
                return res.send(404);
            };
            res.send(200);
        });
    });



    // DELETE MESSAGES
    app.get('/message/delete/:userID/:messageID', function(req,res){
        var reqUserID = req.params.userID;
        console.log(reqUserID);
        var messageID = req.params.messageID;
        console.log(messageID);

        var filter = { "_id": new mongodb.ObjectID(messageID), "userID": reqUserID };

        db.collection('messages').findOneAndUpdate(filter, {
            $set: {
                "deleted": 1
            }
        }, function (err, result) {
            if (err) return res.send(err);
            // res.send(result)
            res.send(200);
        })

    })


    // ===== SINGLE MESSAGE
    // =============================
    app.get('/status/:id' , function(req, res) {

        var reqid = req.params.id;
        // console.log(reqid);

        var filter = { "_id": new mongodb.ObjectID(reqid) };

        // console.log(filter)

        db.collection('messages').find(filter).toArray( function(err, result){
            if(err){
                return res.send(404);
            };
            //BASIC RESULT
            console.log(result);
            // console.log(req.user);

            res.render('message.ejs', {
                user : req.user,
                message : result[0] 
            });
        })
    });

};

