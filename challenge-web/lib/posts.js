import fs from 'fs'
import path from 'path'
// PARSING FILE NAMES
import matter from 'gray-matter'
// PARSING MARKDOWN
import remark from 'remark'
import html from 'remark-html'

// import renderToString from 'next-mdx-remote/render-to-string'
// import hydrate from 'next-mdx-remote/hydrate'

import {
	Heading,
	Text,
	Image,
} from '@chakra-ui/react'

const postsDirectory = path.join(process.cwd(), 'posts')

export function getSortedPostsData() {
	// Get file names under /posts
	const fileNames = fs.readdirSync(postsDirectory)
	const allPostsData = fileNames.map(fileName => {
		// Remove ".md" from file name to get id
		const id = fileName.replace(/\.md$/, '')

		// Read markdown file as string
		const fullPath = path.join(postsDirectory, fileName)
		const fileContents = fs.readFileSync(fullPath, 'utf8')

		// Use gray-matter to parse the post metadata section
		const matterResult = matter(fileContents)

		// Combine the data with the id
		return {
			id,
			...matterResult.data,
		}
	})
	// Sort posts by date
	return allPostsData.sort((a, b) => {
		if (a.date < b.date) {
			return 1
		} else {
			return -1
		}
	})
}

export function getAllPostIds() {
	const fileNames = fs.readdirSync(postsDirectory)

	// Returns an array that looks like this:
	// [
	//   {
	//     params: {
	//       id: 'ssg-ssr'
	//     }
	//   },
	//   {
	//     params: {
	//       id: 'pre-rendering'
	//     }
	//   }
	// ]
	return fileNames.map(fileName => {
		return {
			params: {
				id: fileName.replace(/\.md$/, '')
			}
		}
	})
}

const MyH1 = props => <h1 style={{color: 'green'}} {...props} />

const components = {
	img: Image,
	h1: MyH1,
	h2: Heading.H2,
	p: Text,
	// code: Pre,
	// inlineCode: Code
}

export async function getPostData(id) {
	const fullPath = path.join(postsDirectory, `${id}.md`)
	const fileContents = fs.readFileSync(fullPath, 'utf8')

	// Use gray-matter to parse the post metadata section
	const matterResult = matter(fileContents)

	// console.log(matterResult.content)
	// const mdxSource = await renderToString(matterResult.content, { components })
	// console.log(mdxSource);

	// Use remark to convert markdown into HTML string
	const processedContent = await remark()
		.use(html)
		.process(matterResult.content)
	const contentHtml = processedContent.toString()

	// console.log(processedContent);
	// console.log(contentHtml);

	// Combine the data with the id
	return {
		id,
		...matterResult.data,
		contentHtml,
		// source: mdxSource
	}
}



// export async function getPostDataNextMDX(id) {
// 	const fullPath = path.join(postsDirectory, `${id}.md`)
// 	const fileContents = fs.readFileSync(fullPath, 'utf8')

// 	// Use gray-matter to parse the post metadata section
// 	const matterResult = matter(fileContents)

// 	// Use remark to convert markdown into HTML string
// 	const processedContent = await remark()
// 		.use(html)
// 		.process(matterResult.content)
// 	const contentHtml = processedContent.toString()

// 	console.log(processedContent);
// 	console.log(contentHtml);

// 	// Combine the data with the id
// 	return {
// 		id,
// 		...matterResult.data,
// 		contentHtml,
// 	}
// }

