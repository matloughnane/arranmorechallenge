const groups = [
	{
		"groupA": "Real Lypis",
		"groupB": "Midland Warriors",
		"groupC": "Tory Celtic",
		"groupD": "The Rebels"
	},
	{
		"groupA": "Dungloe Town PLC",
		"groupB": "The Saturdays",
		"groupC": "Gallagher Tunnelling",
		"groupD": "Damo's Dream"
	},
	{
		"groupA": "M.T.F.C.",
		"groupB": "FC Palatico",
		"groupC": "Clannad Celtic",
		"groupD": "-"
	},
	{
		"groupA": "Northern Tunnelling",
		"groupB": "Arranmore Utd",
		"groupC": "Stonecutters",
		"groupD": "Barhale"
	}
];

const names = ["groupA", "groupB", "groupC", "groupD"];

let newGroup = [];

names.forEach((n) => {
	let tempObj = { "group": n };
	for (let index = 0; index < 4; index++) {
		const element = groups[index][n];
		// console.log(element);
		tempObj[index+1] = element
	}
	newGroup.push(tempObj)
})

console.log(newGroup);