import React from 'react';
import Link from 'next/link'

import {
	Table,
	Thead,
	Tbody,
	Tfoot,
	Badge,
	Tr,
	Th,
	Td,
	TableCaption,
} from '@chakra-ui/react'

import { getBadgeColour } from '../../lib/utils';

export default function FlatTable(props) {
	return (
		<>
			<Table variant="striped">
				<TableCaption>Results from the {props.year} Arranmore Challenge</TableCaption>
				<Thead>
					<Tr>
						<Th>Type</Th>
						<Th>Time</Th>
						<Th>Team</Th>
						<Th>Score</Th>
						<Th>Score</Th>
						<Th>Team</Th>
					</Tr>
				</Thead>
				<Tbody>
					{props.results.map((e) =>
						<Tr>
							{/* <Td><Date dateString={e.datetime} /></Td> */}
							<Td><Badge bg={getBadgeColour(e.type)} color={"white"}>{e.type.toUpperCase()}</Badge></Td>
							<Td>{e.datetime} </Td>
							{parseInt(e.score1) + parseInt(e.pen1) + parseInt(e.sd1) > parseInt(e.score2) + parseInt(e.pen2) + parseInt(e.sd2) ? <Td><strong>{e.team1}</strong></Td> : <Td>{e.team1}</Td>}
							<Td><TeamScore score={parseInt(e.score1)} pen={parseInt(e.pen1)} sd={parseInt(e.sd1)} /></Td>
							<Td><TeamScore score={parseInt(e.score2)} pen={parseInt(e.pen2)} sd={parseInt(e.sd2)} /></Td>
							{parseInt(e.score1) + parseInt(e.pen1) + parseInt(e.sd1) < parseInt(e.score2) + parseInt(e.pen2) + parseInt(e.sd2) ? <Td><strong>{e.team2}</strong></Td> : <Td>{e.team2}</Td>}
						</Tr>
					)}
				</Tbody>
				<Tfoot>
					<Tr>
						<Th>Type</Th>
						<Th>Time</Th>
						<Th>Team</Th>
						<Th>Score</Th>
						<Th>Score</Th>
						<Th>Team</Th>
					</Tr>
				</Tfoot>
			</Table>
		</>
	)
}
