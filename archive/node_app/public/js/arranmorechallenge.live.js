
// ==================================
// TESTING
// ==================================

// var endpoint = "http://localhost:2017";
// // var endpoint = "https://live.arranmorechallenge.com";
// var refreshRate = 60*1000;

// ==================================
// TESTING
// ==================================

// console.log(userObj);

// ==================================
// PROFILE
// ==================================

// console.log(userObj.autorefresh);

if (userObj.refreshPreference != undefined) {
	if (userObj.refreshPreference != "false") {
		document.getElementById("refresh-preference").checked = true;
	}
};


// =============================
// TEAM PREFERENCES SECTION
// =============================

$('#my-team').change(function(){
	console.log($('#my-team').val());
});

// =============================
// PREFERENCES SECTION
// =============================
var autoRefresher

// FIRST CHECK
if ($('#refresh-preference').is(':checked')) {
	console.log("Checked on Load")
	autoRefresher = setInterval(function() {
	    // console.log("checked log");
	    getLatestMessages()
	}, refreshRate);
}

// CHECK FOR CHANGES
$('#refresh-preference').change(function(){
    var c = this.checked
    console.log(c);
    // SAVE THIS PREFERENCE
    var prefURL = endpoint+"/user/preference-auto/"+userObj._id+"/"
    // console.log(prefURL);

    if (c == true){
    	var prefURL = prefURL+"true"

    	$.get( prefURL, function( data ) {
		  console.log("preference set to true")
		});

		autoRefresher = setInterval(function() {
		    // console.log("checked log");
		    getLatestMessages()
		}, refreshRate);
    } else {
    	var prefURL = prefURL+"false"
    	clearInterval(autoRefresher);

		$.get( prefURL, function( data ) {
		  console.log("set to false")
		});

    }
});


// POST NEW MESSAGE
$('#sendMessage').click(function(){
	
	if( $('#messageInput').val() == "" ) {
		// do nothing
	} else {

		// CONSTRUCT MESSAGE
		var message = {}
		message.userID = userObj._id;
		if (userObj.local){
			message.username = userObj.local.username;
		} else {
			message.username = userObj.twitter.username;
			// message.userProfileImage = userObj.twitter.profileImage;
		}
		message.adminStatus = userObj.admin;
		message.content = $('#messageInput').val();
		var date = new Date();
		var edate = date.getTime();
		message.timestamp = parseInt(edate);

		// console.log(message);
		// edit here to log out

		// })
		$.post( endpoint+'/update/post', message)
			.done(function( data ) {
				console.log(data)
				if (data == "OK"){
					$('#messageInput').val("");
					getLatestMessages()
				} else {
					alert("Your message didn't send");
				}
		});

	}

})



