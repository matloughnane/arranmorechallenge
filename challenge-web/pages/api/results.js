// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { results } from "../../lib/results";

export default (req, res) => {
  res.status(200).json({ "results": results })
}
