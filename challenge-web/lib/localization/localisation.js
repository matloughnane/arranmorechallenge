import { gaeilge } from './gaeilge';
import { english } from './english';

export function getString(stringID, language) {
	if (language === "ga") {
		return gaeilge[stringID];
	} else {
		return english[stringID];
	}
}