import {
	Stack,
	VStack,
	Flex,
	Text,
	Button,
	useBreakpointValue
} from '@chakra-ui/react'

import Link from 'next/link'

// PROPS
// image
// headline
// link
// linkText

export default function ImageDivider(props) {

	return (<Flex
		w={'full'}
		h={'40vh'}
		backgroundImage={
			props.image ? `url(${props.image})` : 'url(/images/backgrounds/pitch.jpg)'
		}
		backgroundSize={'cover'}
		backgroundPosition={'center center'}>
		<VStack
			w={'full'}
			justify={'center'}
			px={useBreakpointValue({ base: 4, md: 8 })}
			bgGradient={'linear(to-r, blackAlpha.600, transparent)'}>
			<Stack maxW={'2xl'} align={'flex-start'} spacing={6}>
				<Text
					color={'white'}
					fontWeight={700}
					lineHeight={1.2}
					fontSize={useBreakpointValue({ base: '1xl', md: '2xl' })}>
					{props.headline ? `${props.headline}` : 'Arranmore Challenge'}
				</Text>
				<Stack direction={'row'}>
					{props.link ? <Link href={props.link}><Button
						bg={'primary.400'}
						rounded={'lg'}
						color={'white'}
						_hover={{ bg: 'primary.100' }}>
						{props.linkText ? props.linkText : "Learn More"}
					</Button></Link> : <></>}

				</Stack>
			</Stack>
		</VStack>
	</Flex>)
}