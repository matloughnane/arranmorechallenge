
// ==================================
// TESTING
// ==================================

var endpoint = "https://live.arranmorechallenge.com";
// var endpoint = "http://localhost:2017";
var refreshRate = 60*1000;

// ==================================
// TESTING
// ==================================

// console.log(userObj);

// ==================================
// PROFILE
// ==================================

// console.log(userObj.autorefresh);

if (userObj.refreshPreference != undefined) {
	if (userObj.refreshPreference != "false") {
		document.getElementById("refresh-preference").checked = true;
	}
};


// =============================
// TEAM PREFERENCES SECTION
// =============================

$('#my-team').change(function(){
	console.log($('#my-team').val());
});

// =============================
// PREFERENCES SECTION
// =============================
var autoRefresher

// FIRST CHECK
// if ($('#refresh-preference').is(':checked')) {
// 	console.log("Checked on Load")
// 	autoRefresher = setInterval(function() {
// 	    // console.log("checked log");
// 	   getLatestMessages()
// 	}, refreshRate);
// }

// CHECK FOR CHANGES
$('#refresh-preference').change(function(){
    var c = this.checked
    console.log(c);
    // SAVE THIS PREFERENCE
    var prefURL = endpoint+"/user/preference-auto/"+userObj._id+"/"
    // console.log(prefURL);

    if (c == true){
    	var prefURL = prefURL+"true"

    	$.get( prefURL, function( data ) {
		  console.log("preference set to true")
		});

		autoRefresher = setInterval(function() {
		    // console.log("checked log");
		    getLatestMessages()
		}, refreshRate);
    } else {
    	var prefURL = prefURL+"false"
    	clearInterval(autoRefresher);

		$.get( prefURL, function( data ) {
		  console.log("set to false")
		});

    }
});


// // POST NEW MESSAGE
// $('#sendMessage').click(function(){
	
// 	if( $('#messageInput').val() == "" ) {
// 		// do nothing
// 	} else {

// 		// CONSTRUCT MESSAGE
// 		var message = {}
// 		message.userID = userObj._id;
// 		if (userObj.local){
// 			message.username = userObj.local.username;
// 		} else {
// 			message.username = userObj.twitter.username;
// 			// message.userProfileImage = userObj.twitter.profileImage;
// 		}
// 		message.adminStatus = userObj.admin;
// 		message.content = $('#messageInput').val();
// 		var date = new Date();
// 		var edate = date.getTime();
// 		message.timestamp = edate;

// 		// console.log(message);

// 		// })
// 		$.post( endpoint+'/update/post', message)
// 			.done(function( data ) {
// 				console.log(data)
// 				if (data == "OK"){
// 					$('#messageInput').val("");
// 					getLatestMessages()
// 				} else {
// 					alert("Your message didn't send");
// 				}
// 		});

// 	}

// })


loadProfileMessages();

function loadProfileMessages(){
	// $.post( endpoint+'/update/post', message).done(function( data ) {
	// 		console.log(data)
	// 		if (data == "OK"){
	// 			$('#messageInput').val("");
	// 			getLatestMessages()
	// 		} else {
	// 			alert("Your message didn't send");
	// 		}
	// });
	// console.log(userObj);

	$.get( endpoint+'/profileMessages/'+userObj._id ).done(function( data ) {
		// console.log("initial load");
		console.log(data);
		if(data.messages.length == 0 ){
			$("#messagesProfileContainer").html("");
			$('#messagesProfileContainer').html('<div class="align-center">You have not posted anything yet, useless!</div>');
		} else {
			// do nothing yet
			$("#messagesProfileContainer").html("");
			for (var i = data.messages.length - 1; i >= 0; i--) {
				// console.log(data.messages[i]);
				if (data.messages[i].adminStatus == "true"){
					var adminMessageHTML = constructMyProfileMessage(data.messages[i])
					/// prepend to messages
					$("#messagesProfileContainer").prepend(adminMessageHTML);
				} else {
					var fanMessageHTML = constructMyProfileMessage(data.messages[i])
					$("#messagesProfileContainer").prepend(fanMessageHTML);
				}
			}
			// HIDE THE LATEST MESSAGE ID
			$('#latestMessageTime').val(data.messages[0].timestamp);
		}

		setMessageDeleteListeners();
		
	});

};


function getTimeString(epoch){
	var date = new Date(parseInt(epoch));
	// console.log(date);
	var hours = lessThanTen(date.getHours());
	var minutes = lessThanTen(date.getMinutes());

	var stringDate = lessThanTen(date.getDate());
	var stringMonth = lessThanTen(date.getMonth()+1);

	var string = hours+":"+minutes+" "+stringDate+"/"+stringMonth;
	return string;
}

function lessThanTen(number){
	if (number < 10){
		return "0"+number
	} else {
		return number
	}
}


function constructMyProfileMessage(messageObj){

	var html = '<div class="unit w-1-1 card plain card-message admin">';
	html += '<div class="message-delete-button" id="'+messageObj._id+'">DELETE</div>';
	html += '<div class="profile-icon" id="profile_'+messageObj.userID+'"></div>';
	// if (messageObj.userProfileImage){
	// 	html += '<img src="'+messageObj.userProfileImage+'" />';
	// }

	var timestamp = getTimeString(messageObj.timestamp);

	html += '<div class="message-text-admin"><a href="/profile/'+messageObj.userID+'">';
	html += '<h3 class="username">'+messageObj.username+'</h3> </a><br>';
	html += '<span class="status">'+messageObj.content+'</span></div>';
	html += '<div class="message-share-admin">';
	html += '<span class="message-timestamp">'+timestamp+'</span>';
	html += '<a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//live.arranmorechallenge.com/status/'+messageObj._id+'" target="_blank" class="message-share-button"><i class="icon-facebook"></i></a>';
	html += '<a href="https://twitter.com/home?status=http%3A//live.arranmorechallenge.com/status/'+messageObj._id+'" target="_blank" class="message-share-button"><i class="icon-twitter"></i></a> ';
	html += '<a href="/status/'+messageObj._id+'" target="_blank" class="message-share-button"><i class="icon-export"></i></a>';
	html += '</div></div>';

	return html
};


// DELETE ACCOUNT

// $('#deleteAccount').click(function(){
// 	var userID = userObj._id;
// 	console.log(userID);

// 	swal({
// 	  title: "Are you sure you want to delete your account?",
// 	  text: "All your messages will be deleted and you will not be able to recover it.",
// 	  type: "warning",
// 	  showCancelButton: true,
// 	  confirmButtonColor: "#DD6B55",
// 	  confirmButtonText: "Yes, delete it!",
// 	  cancelButtonText: "No, cancel it!",
// 	  closeOnConfirm: false,
// 	  closeOnCancel: false
// 	},
// 	function(isConfirm){
// 	  if (isConfirm) {
// 	    swal("Deleted!", "Your account is now deleted.", "success");
// 	    window.location.replace("http://live.arranmorechallenge.com");
// 	  } else {
// 	    swal("Cancelled", "Your account and messages is still online", "error");
// 	  }
// 	});

// });



function setMessageDeleteListeners(){
	// console.log("listeners set");
	var userID = userObj._id;

	$('.message-delete-button').click(function(){
		// console.log("click");
		// var messageID = ;
		var messageID = $(this).attr('id');
		// console.log(messageID)

		swal({
		  title: "Are you sure you want to delete this message?",
		  text: "All your messages will be deleted and you will not be able to recover it.",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "No, cancel it!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm){
		  if (isConfirm) {
		  	
		  	//  CONFIRMED
		  	var delURL = endpoint+"/message/delete/"+userID+"/"+messageID

  			$.get( delURL, function( data ) {
			  // console.log("preference set to true")
			  // console.log("MESSAGE DELETED");
			});

		    swal("Deleted!", "Your message has now been deleted.", "success");
		    loadProfileMessages();
		    // window.location.replace("http://live.arranmorechallenge.com");
		  } else {
		  	// CANCELLED
		    swal("Cancelled", "Your message is still online", "error");
		  }
		});
	});

};


