// status updates.js

// SIGNUPS

function validateSignup() {
	var e = $('#signupEmail').val();
	var u = $('#signupUsername').val();
	var p = $('#signupPassword').val();

	// CHECK FOR CONTENT
	if (e == "") {
		errorMessage("You need to provide a email");
		return false;
	} else if (u == "") {
		errorMessage("You need to provide a valid username");
		return false;
	} else if (p == "") {
		errorMessage("You need a password");
		return false;
	}

	// CHECK FOR VALID EMAIL
    var atpos = e.indexOf("@");
    var dotpos = e.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=e.length) {
        errorMessage("Not a valid e-mail address");
        return false;
    }

}


// ERROR MESSAGE
function errorMessage(text){
	document.getElementById("signupMessage").innerHTML = text;
}

