import Head from 'next/head'
import styles from '../styles/Home.module.css'

// import Link from 'next/link'
import React from 'react';

import { getSortedPostsData } from '../lib/posts'
import { Heading, Box, Container, Stack, Badge, Text, useColorModeValue } from '@chakra-ui/react'
// import Date from '../components/date'
import NavBar from '../components/common/navbar'
import Footer from '../components/common/footer'
import HomePageHero from '../components/hero'
import HomePageChampionsHero from '../components/champions'
import HomePageFeature from '../components/feature'
import WithSpeechBubbles from '../components/testimonials'
import MetaHead from '../components/common/metaHead';

export function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}

export default function Home({ allPostsData }) {
  return (

    <div>
      <MetaHead title={"Arranmore Island's Football Competition"} />

      <NavBar />

      <HomePageHero />

      <HomePageChampionsHero />

      <HomePageFeature />

      {/* <WithSpeechBubbles /> */}

      <Box bg={useColorModeValue('white', 'gray.800')}>
        <Container maxW={'7xl'} py={16} as={Stack} spacing={12}>
          <Stack spacing={0} align={'center'}>
            <Heading>The Arranmore Challenge App</Heading>
            <Text>Coming Soon...</Text>
          </Stack>
        </Container>
      </Box>

      <main className={styles.main}>
        {/* <Heading size="xl" mb="5">Welcome to Mat's Next.JS Website</Heading> */}

        {/* <p className={styles.description}>
          Get started by editing{' '}
          <code className={styles.code}>pages/index.js</code>
        </p> */}

        {/* <div className={styles.grid}>
          <Link href="/posts/first-post">
            <h1 className="title" className={styles.card}>
              Read{' '}
              <a>this page!</a>
            </h1>
          </Link>

          <a href="https://nextjs.org/learn" className={styles.card}>
            <h3>Learn &rarr;</h3>
            <p>Learn about Next.js in an interactive course with quizzes!</p>
          </a>

          <a
            href="https://github.com/vercel/next.js/tree/master/examples"
            className={styles.card}
          >
            <h3>Examples &rarr;</h3>
            <p>Discover and deploy boilerplate example Next.js projects.</p>
          </a>

          <a
            href="https://vercel.com/new?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
            className={styles.card}
          >
            <h3>Deploy &rarr;</h3>
            <p>
              Instantly deploy your Next.js site to a public URL with Vercel.
            </p>
          </a>
        </div> */}


        {/* <section className={"blog"} >
          {/* <h2 className={utilStyles.headingLg}>Blog</h2> */}
        {/* <Heading size="lg">Blog</Heading> */}
        {/* <ul >
            {allPostsData.map(({ id, date, title }) => (
              <li key={id}>
                {title}
                <br />
                {id}
                <br />
                {date}
              </li>
            ))}
          </ul> 
          {allPostsData.map(({ id, date, title }) => (
            <Link href={`/posts/${id}`} key={id} >
              <Box maxW="sm" borderWidth="1px" mb="4" borderRadius="lg" overflow="hidden" style={{ cursor: "pointer" }}>
                <Box p="6">
                  <Box mt="1" fontWeight="semibold" as="h4" lineHeight="7" > {title} </Box>

                  <Box d="flex" alignItems="baseline">
                    <Badge borderRadius="full" px="2" colorScheme="teal">
                      <Date dateString={date} />
                    </Badge>
                  </Box>

                  <Text>ID: {id}</Text>

                </Box>
              </Box>
            </Link>
          ))}
        </section> */}
      </main>

      <Footer />

    </div>
  )
}
