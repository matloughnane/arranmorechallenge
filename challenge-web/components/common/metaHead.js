import Head from 'next/head';

export default function MetaHead(props) {
	const date = new Date();
	// const dateTime = new Date(dateString).toString()
	// 	return (<Head>
	// <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	// 	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	// 		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	// 			<link rel="manifest" href="/site.webmanifest"></link>
	//   </Head>)
	return (
		<Head>
			<title>{props.title} | Arranmore Challenge</title>
			{/* FAVICON */}
			<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png" />
			<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png" />
			<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png" />
			<link rel="manifest" href="/favicon/site.webmanifest" />
			{/* META */}
			<meta name="keywords" content="Arranmore, Island, Football, Competition, June, Annual" />
			<meta name="description" content="The Arranmore Challenge is an annual football competition held on Arranmore Island over the June Bank Holiday weekend" />
			<meta name="subject" content="The Arranmore Challenge Competition" />
			<meta name="copyright" content="Hexa Studios" />
			<meta name="language" content="EN" />
			<meta name="robots" content="index,follow" />
			<meta name="revised" content={date.toISOString()} />
			<meta name="abstract" content="" />
			<meta name="topic" content="" />
			<meta name="summary" content="" />
			<meta name="Classification" content="Business" />
			{/* <meta name="author" content="name, email@hotmail.com" /> */}
			{/* <meta name="designer" content="" /> */}
			{/* <meta name="copyright" content="" /> */}
			{/* <meta name="reply-to" content="mat@hexastudios.co" /> */}
			<meta name="owner" content="" />
			<meta name="url" content={props.url === undefined ? "https://arranmorechallenge.com" : "https://arranmorechallenge.com" + props.url} />
			<meta name="identifier-URL" content={props.url === undefined ? "https://arranmorechallenge.com" : "https://arranmorechallenge.com" + props.url} />
			<meta name="directory" content="submission" />
			<meta name="category" content="" />
			<meta name="coverage" content="Worldwide" />
			<meta name="distribution" content="Global" />
			{/* OPEN GRAPH */}
			<meta name="og:title" content={props.title} />
			<meta name="og:type" content="website" />
			<meta name="og:url" content={props.url === undefined ? "https://arranmorechallenge.com" : "https://arranmorechallenge.com" + props.url} />
			<meta name="og:image" content={props.image === undefined ? "https://arranmorechallenge.com/images/backgrounds/social.jpg" : props.image} />
			<meta name="og:site_name" content="Arranmore Challenge" />
			<meta name="og:description" content="The Arranmore Challenge is an annual football competition held on Arranmore Island over the June Bank Holiday weekend" />

			{/* <meta name="fb:page_id" content="43929265776" /> */}

			<meta name="og:email" content="mat@hexastudios.co" />
			{/* <meta name="og:phone_number" content="650-123-4567" /> */}
			{/* <meta name="og:fax_number" content="+1-415-123-4567" /> */}
			{/* 54.971401, -8.524414 */}
			<meta name="og:latitude" content="54.971401" />
			<meta name="og:longitude" content="-8.524414" />
			<meta name="og:street-address" content="Rannagh Park" />
			<meta name="og:locality" content="Arranmore Island" />
			<meta name="og:region" content="Donegal" />
			{/* <meta name="og:postal-code" content="94304" /> */}
			<meta name="og:country-name" content="Ireland" />

			{/* <meta property="og:type" content="game.achievement" />
			<meta property="og:points" content="POINTS_FOR_ACHIEVEMENT" /> */}

			{/* <meta property="og:video" content="http://example.com/awesome.swf" />
			<meta property="og:video:height" content="640" />
			<meta property="og:video:width" content="385" />
			<meta property="og:video:type" content="application/x-shockwave-flash" />
			<meta property="og:video" content="http://example.com/html5.mp4" />
			<meta property="og:video:type" content="video/mp4" />
			<meta property="og:video" content="http://example.com/fallback.vid" />
			<meta property="og:video:type" content="text/html" />

			<meta property="og:audio" content="http://example.com/amazing.mp3" />
			<meta property="og:audio:title" content="Amazing Song" />
			<meta property="og:audio:artist" content="Amazing Band" />
			<meta property="og:audio:album" content="Amazing Album" />
			<meta property="og:audio:type" content="application/mp3" /> */}
		</Head>
	)
}

