//  ========================================================
//  ====== FUNCTIONS
//  ========================================================
export function getHistoricalWinners() {
    return historicalWinners;
}

export function getArchivedYears() {
    const archiveList = [];
    results.map((r) => archiveList.push({ year: r.year }));
    archiveList.sort((a, b) => {
        return b.year - a.year;
    });
    return archiveList;
}

export function getYearParams() {
    const archiveList = [];
    results.map((r) => archiveList.push({ params: { year: r.year } }));
    return archiveList;
}

export function getYearData(year) {
    const yearResult = results.filter((r) => r.year === year);
    // console.log(yearResult);
    return yearResult[0];
}

//  ========================================================
//  ======  CONSTANTS
//  ========================================================

// HISTORICAL
export const historicalWinners = [
    {
        key: "2001",
        year: "2001",
        winners: "Clannad Celtic",
        runners: "Arranmore Utd"
    },
    {
        key: "2002",
        year: "2002",
        winners: "Gallaghers Tunnelling",
        runners: "Arranmore Utd"
    },
    {
        key: "2003",
        year: "2003",
        winners: "Balally",
        runners: "Arranmore Utd"
    },
    {
        key: "2004",
        year: "2004",
        winners: "Balally",
        runners: "Woodcarvers"
    },
    {
        key: "2005",
        year: "2005",
        winners: "Gallaghers Tunnelling",
        runners: "Arranmore Utd"
    },
    {
        key: "2006",
        year: "2006",
        winners: "Arranmore Utd",
        runners: "Gallaghers Tunnelling"
    },
    {
        key: "2007",
        year: "2007",
        winners: "Tory Celtic",
        runners: "Stonecutters"
    },
    {
        key: "2008",
        year: "2008",
        winners: "Gallaghers Tunnelling",
        runners: "Arranmore Utd"
    },
    {
        key: "2009",
        year: "2009",
        winners: "Balally",
        runners: "Woodcarvers"
    },
    {
        key: "2010",
        year: "2010",
        winners: "Gallaghers Tunnelling",
        runners: "Woodcarvers"
    },
    {
        key: "2011",
        year: "2011",
        winners: "Damo's Dream",
        runners: "Real Lypis"
    },
    {
        key: "2012",
        year: "2012",
        winners: "Damo's Dream",
        runners: "Arranmore Utd"
    },
    {
        key: "2013",
        year: "2013",
        winners: "Real Lypis",
        runners: "Damo's Dream"
    },
    {
        key: "2014",
        year: "2014",
        winners: "Finn Harps",
        runners: "Real Lypis"
    },
    {
        key: "2015",
        year: "2015",
        winners: "Damo's Dream",
        runners: "Real Lypis"
    },
    {
        key: "2016",
        year: "2016",
        winners: "Real Lypis",
        runners: "FC Palatico"
    },
    {
        key: "2017",
        year: "2017",
        winners: "Damo's Dream",
        runners: "Real Lypis"
    },
    {
        key: "2018",
        year: "2018",
        winners: "Damo's Dream",
        runners: "FC Palatico"
    },
    {
        key: "2019",
        year: "2019",
        winners: "Dungloe Town PLC",
        runners: "The Rebels"
    },
    {
        key: "2020",
        year: "2020",
        winners: "Postponed Due To COVID",
        runners: ""
    },
    {
        key: "2021",
        year: "2021",
        winners: "Postponed Due To COVID",
        runners: ""
    },
    {
        key: "2022",
        year: "2022",
        winners: "Damo's Dream",
        runners: "Arranmore Utd"
    },
    {
        key: "2023",
        year: "2023",
        winners: "The All Backs",
        runners: "Zoo FC"
    }
];

const results2013 = {
    year: "2013",
    type: "competition",
    groups: null,
    results: [
        {
            type: "group",
            datetime: "Qualifier 1",
            team1: "Damo's Dream",
            score1: "2",
            score2: "1",
            team2: "FC Internazionale"
        },
        {
            type: "group",
            datetime: "Qualifier 2",
            team1: "Gallagher Tunelling",
            score1: "0",
            score2: "3",
            team2: "Real Lypis"
        },
        {
            type: "group",
            datetime: "Qualifier 3",
            team1: "Tory Celtic",
            score1: "0",
            score2: "1",
            team2: "Stonecutters"
        },
        {
            type: "group",
            datetime: "Qualifier 4",
            team1: "Barhale",
            score1: "0",
            score2: "1",
            team2: "Arranmore Utd"
        },
        {
            type: "group",
            datetime: "Qualifier 5",
            team1: "The Rebels",
            score1: "1",
            score2: "1",
            team2: "Alva All Stars"
        },
        {
            type: "group",
            datetime: "Qualifier 6",
            team1: "Glasgow Gaels",
            score1: "0",
            score2: "2",
            team2: "FC Palatico"
        },
        {
            type: "group",
            datetime: "Qualifier 7",
            team1: "Clannad Celtic",
            score1: "2",
            score2: "1",
            team2: "Balally Celtic"
        },
        {
            type: "group",
            datetime: "Qualifier 8",
            team1: "Midland Warriors",
            score1: "2",
            score2: "1",
            team2: "Woodcarvers"
        },
        {
            type: "group",
            datetime: "Qualifier 9",
            team1: "Damo's Dream",
            score1: "0",
            score2: "1",
            team2: "Gallagher Tunelling"
        },
        {
            type: "group",
            datetime: "Qualifier 10",
            team1: "FC Internazionale",
            score1: "0",
            score2: "2",
            team2: "Real Lypis"
        },
        {
            type: "group",
            datetime: "Qualifier 11",
            team1: "Tory Celtic",
            score1: "1",
            score2: "0",
            team2: "Barhale"
        },
        {
            type: "group",
            datetime: "Qualifier 12",
            team1: "Stonecutters",
            score1: "1",
            score2: "1",
            team2: "Arranmore Utd"
        },
        {
            type: "group",
            datetime: "Qualifier 13",
            team1: "The Rebels",
            score1: "2",
            score2: "1",
            team2: "Glasgow Gaels"
        },
        {
            type: "group",
            datetime: "Qualifier 14",
            team1: "Alva All Stars",
            score1: "0",
            score2: "2",
            team2: "FC Palatico"
        },
        {
            type: "group",
            datetime: "Qualifier 15",
            team1: "Clannad Celtic",
            score1: "1",
            score2: "0",
            team2: "Midland Warriors"
        },
        {
            type: "group",
            datetime: "Qualifier 16",
            team1: "Balally Celtic",
            score1: "4",
            score2: "2",
            team2: "Woodcarvers"
        },
        {
            type: "group",
            datetime: "Qualifier 17",
            team1: "Damo's Dream",
            score1: "0",
            score2: "6",
            team2: "Real Lypis"
        },
        {
            type: "group",
            datetime: "Qualifier 18",
            team1: "FC Internazionale",
            score1: "-",
            score2: "-",
            team2: "Gallagher Tunelling"
        },
        {
            type: "group",
            datetime: "Qualifier 19",
            team1: "Tory Celtic",
            score1: "0",
            score2: "4",
            team2: "Arranmore Utd"
        },
        {
            type: "group",
            datetime: "Qualifier 20",
            team1: "Stonecutters",
            score1: "2",
            score2: "2",
            team2: "Barhale"
        },
        {
            type: "group",
            datetime: "Qualifier 21",
            team1: "The Rebels",
            score1: "0",
            score2: "2",
            team2: "FC Palatico"
        },
        {
            type: "group",
            datetime: "Qualifier 22",
            team1: "Alva All Stars",
            score1: "1",
            score2: "0",
            team2: "Glasgow Gaels"
        },
        {
            type: "group",
            datetime: "Qualifier 23",
            team1: "Clannad Celtic",
            score1: "2",
            score2: "5",
            team2: "Woodcarvers"
        },
        {
            type: "group",
            datetime: "Qualifier 24",
            team1: "Balally Celtic",
            score1: "2",
            score2: "0",
            team2: "Midland Warriors"
        },
        {
            type: "quarter",
            datetime: "Final 1",
            team1: "Clannad Celtic",
            score1: "1",
            pen1: 6,
            score2: "1",
            pen2: 7,
            team2: "Real Lypis"
        },
        {
            type: "quarter",
            datetime: "Final 2",
            team1: "Damo's Dream",
            score1: "0",
            pen1: 5,
            score2: "0",
            pen2: 4,
            team2: "Balaly"
        },
        {
            type: "quarter",
            datetime: "Final 3",
            team1: "FC Palatico",
            score1: "1",
            score2: "2",
            team2: "Stonecutters"
        },
        {
            type: "quarter",
            datetime: "Final 4",
            team1: "Arranmore Utd",
            score1: "3",
            score2: "0",
            team2: "The Rebels"
        },
        {
            type: "semi",
            datetime: "Semi-Final 1",
            team1: "Damo's Dream",
            score1: "1",
            score2: "0",
            team2: "Stonecutters"
        },
        {
            type: "semi",
            datetime: "Semi-Final 2",
            team1: "Real Lypis",
            score1: "3",
            score2: "0",
            team2: "Arranmore Utd"
        },
        {
            type: "final",
            datetime: "The Final",
            team1: "Real Lypis",
            score1: "3",
            score2: "2",
            team2: "Damo's Dream"
        }
    ]
};

const results2014 = {
    year: "2014",
    type: "competition",
    groups: [
        {
            1: "Real Lypis ",
            2: "Tory Celtic ",
            3: "Glasgow Gaels ",
            4: "Ballaly ",
            group: "groupA"
        },
        {
            1: "St Annes United ",
            2: "Gallagher Tunnelling ",
            3: "Arranmore ",
            4: "The Rebels ",
            group: "groupB"
        },
        {
            1: "Midland Warriors ",
            2: "F.C. Palatico ",
            3: "Clannad Celtic ",
            4: "F.C. Internazionale ",
            group: "groupC"
        },
        {
            1: "Finn Harps Youths",
            2: "Stonecutters",
            3: "Barhale",
            4: "Damo's Dream",
            group: "groupD"
        }
    ],
    results: [
        {
            type: "group",
            datetime: "31/05 11:30",
            team1: "Real Lypis",
            score1: "1",
            score2: "0",
            team2: "Tory Celtic"
        },
        {
            type: "group",
            datetime: "31/05 11:55",
            team1: "Glasgow Gaels",
            score1: "0",
            score2: "6",
            team2: "Balally Celtic"
        },
        {
            type: "group",
            datetime: "31/05 12:20",
            team1: "St Annes United",
            score1: "0",
            score2: "4",
            team2: "Gallagher Tunnelling"
        },
        {
            type: "group",
            datetime: "31/05 12:45",
            team1: "Arranmore Utd",
            score1: "0",
            score2: "0",
            team2: "The Rebels"
        },
        {
            type: "group",
            datetime: "31/05 13:10",
            team1: "Midland Warrior",
            score1: "1",
            score2: "0",
            team2: "FC Palatico"
        },
        {
            type: "group",
            datetime: "31/05 13:35",
            team1: "Clannad Celtic",
            score1: "WON",
            score2: "-",
            team2: "FC Internazionale"
        },
        {
            type: "group",
            datetime: "31/05 14:00",
            team1: "Finn Harps Youths (Woodcarvers)",
            score1: "WON",
            score2: "-",
            team2: "Barhale"
        },
        {
            type: "group",
            datetime: "31/05 14:25",
            team1: "Damo's Dream",
            score1: "-",
            score2: "WON",
            team2: "Stonecutters"
        },
        {
            type: "group",
            datetime: "31/05 14:50",
            team1: "Real Lypis",
            score1: "4",
            score2: "0",
            team2: "Glasgow Gaels"
        },
        {
            type: "group",
            datetime: "31/05 15:15",
            team1: "Tory Celtic",
            score1: "1",
            score2: "1",
            team2: "Balally Celtic"
        },
        {
            type: "group",
            datetime: "31/05 15:40",
            team1: "St Annes United",
            score1: "0",
            score2: "1",
            team2: "Arranmore United"
        },
        {
            type: "group",
            datetime: "31/05 16:05",
            team1: "Gallagher Tunnelling",
            score1: "1",
            score2: "0",
            team2: "The Rebels"
        },
        {
            type: "group",
            datetime: "31/05 16:30",
            team1: "Midland Warriors",
            score1: "3",
            score2: "2",
            team2: "Clannad Celtic"
        },
        {
            type: "group",
            datetime: "31/05 16:55",
            team1: "FC Palatico",
            score1: "1",
            score2: "1",
            team2: "FC Internazionale"
        },
        {
            type: "group",
            datetime: "31/05 17:20",
            team1: "Finn Harps Youths (Woodcarvers)",
            score1: "3",
            score2: "1",
            team2: "Damo's Dream"
        },
        {
            type: "group",
            datetime: "31/05 17:45",
            team1: "Barhale",
            score1: "1",
            score2: "4",
            team2: "Stonecutters"
        },
        {
            type: "group",
            datetime: "01/06 11:30",
            team1: "Real Lypis",
            score1: "1",
            score2: "0",
            team2: "Balally Celtic"
        },
        {
            type: "group",
            datetime: "01/06 11:55",
            team1: "Tory Celtic",
            score1: "7",
            score2: "0",
            team2: "Glasgow Gaels"
        },
        {
            type: "group",
            datetime: "01/06 12:20",
            team1: "St Annes United",
            score1: "1",
            score2: "3",
            team2: "The Rebels"
        },
        {
            type: "group",
            datetime: "01/06 12:45",
            team1: "Gallagher Tunnelling",
            score1: "1",
            score2: "0",
            team2: "Arranmore United"
        },
        {
            type: "group",
            datetime: "01/06 13:10",
            team1: "Midland Warriors",
            score1: "2",
            score2: "2",
            team2: "FC Internazionale"
        },
        {
            type: "group",
            datetime: "01/06 13:35",
            team1: "FC Palatico",
            score1: "1",
            score2: "3",
            team2: "Clannad Celtic"
        },
        {
            type: "group",
            datetime: "01/06 14:00",
            team1: "Finn Harps Youths (Woodcarvers)",
            score1: "1",
            score2: "2",
            team2: "Stonecutters"
        },
        {
            type: "group",
            datetime: "01/06 14:25",
            team1: "Barhale",
            score1: "3",
            score2: "1",
            team2: "Damo's Dream"
        },
        {
            type: "quarter",
            datetime: "Quarter 1",
            team1: "Real Lypis",
            score1: "1",
            pen1: 3,
            score2: "1",
            pen2: 2,
            team2: "Clannad Celtic"
        },
        {
            type: "quarter",
            datetime: "Quarter 2",
            team1: "Midland Warriors",
            score1: "0",
            score2: "3",
            team2: "Balally"
        },
        {
            type: "quarter",
            datetime: "Quarter 3",
            team1: "Stonecutters",
            score1: "1",
            pen1: 3,
            score2: "1",
            pen2: 2,
            team2: "The Rebels"
        },
        {
            type: "quarter",
            datetime: "Quarter 4",
            team1: "Finn Harps (Woodcarvers)",
            score1: "2 ",
            pen1: 2,
            score2: "2",
            pen2: 2,
            team2: "Gallagher's Tunneling"
        },
        {
            type: "semi",
            datetime: "Semi 1",
            team1: "Real Lypis",
            score1: "1",
            score2: "0",
            team2: "The Rebels"
        },
        {
            type: "semi",
            datetime: "Semi 2",
            team1: "Balally",
            score1: "0",
            score2: "3",
            team2: "Finn Harps (Woodcarvers)"
        },
        {
            type: "final",
            datetime: "FINAL",
            team1: "Finn Harps",
            score1: "0",
            pen1: 3,
            score2: "0",
            pen2: 2,
            team2: "Real Lypis"
        }
    ]
};

const results2018 = {
    year: "2018",
    type: "competition",
    results: [
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 11:30",
            team1: "Damo's Dream",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 2,
            pen2: 0,
            sd2: 0,
            team2: "Midland Warriors"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 11:55",
            team1: "Barhale",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 1,
            pen2: 0,
            sd2: 0,
            team2: "Dungloe Town PLC"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 12:20",
            team1: "Gallagher Tunnelling",
            score1: 2,
            pen1: 0,
            sd1: 0,
            score2: 1,
            pen2: 0,
            sd2: 0,
            team2: "Bayern Neverlusen"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 12:45",
            team1: "FC Internazionale",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 2,
            pen2: 0,
            sd2: 0,
            team2: "The Rebels"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 13:10",
            team1: "Arranmore Utd",
            score1: 6,
            pen1: 0,
            sd1: 0,
            score2: 0,
            pen2: 0,
            sd2: 0,
            team2: "Clannad Celtic"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 13:35",
            team1: "Northern Tunnelling",
            score1: 1,
            pen1: 0,
            sd1: 0,
            score2: 3,
            pen2: 0,
            sd2: 0,
            team2: "FC Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 14:00",
            team1: "Tory Celtic",
            score1: 2,
            pen1: 0,
            sd1: 0,
            score2: 0,
            pen2: 0,
            sd2: 0,
            team2: "FC Shaktar"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 14:25",
            team1: "Luton Irish",
            score1: 1,
            pen1: 0,
            sd1: 0,
            score2: 2,
            pen2: 0,
            sd2: 0,
            team2: "Stonecutters"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 14:50",
            team1: "Damo's Dream",
            score1: 3,
            pen1: 0,
            sd1: 0,
            score2: 0,
            pen2: 0,
            sd2: 0,
            team2: "Barhale"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 15:15",
            team1: "Midland Warriors",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 0,
            pen2: 0,
            sd2: 0,
            team2: "Dungloe Town PLC"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 15:40",
            team1: "Gallagher Tunnelling",
            score1: 2,
            pen1: 0,
            sd1: 0,
            score2: 0,
            pen2: 0,
            sd2: 0,
            team2: "FC Internazionale"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 16:05",
            team1: "Bayern Neverlusen",
            score1: 1,
            pen1: 0,
            sd1: 0,
            score2: 1,
            pen2: 0,
            sd2: 0,
            team2: "The Rebels"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 16:30",
            team1: "Arranmore Utd",
            score1: 1,
            pen1: 0,
            sd1: 0,
            score2: 2,
            pen2: 0,
            sd2: 0,
            team2: "Northern Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 16:55",
            team1: "Clannad Celtic",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 6,
            pen2: 0,
            sd2: 0,
            team2: "FC Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 17:20",
            team1: "Tory Celtic",
            score1: 3,
            pen1: 0,
            sd1: 0,
            score2: 2,
            pen2: 0,
            sd2: 0,
            team2: "Luton Irish"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/01 17:55",
            team1: "FC Shaktar",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 3,
            pen2: 0,
            sd2: 0,
            team2: "Stonecutters"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/02 11:30",
            team1: "Damo's Dream",
            score1: 2,
            pen1: 0,
            sd1: 0,
            score2: 0,
            pen2: 0,
            sd2: 0,
            team2: "Dungloe Town PLC"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/02 11:55",
            team1: "Midland Warriors",
            score1: 2,
            pen1: 0,
            sd1: 0,
            score2: 1,
            pen2: 0,
            sd2: 0,
            team2: "Barhale"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/02 12:20",
            team1: "Gallagher Tunnelling",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 3,
            pen2: 0,
            sd2: 0,
            team2: "The Rebels"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/02 12:45",
            team1: "Bayern Neverlusen",
            score1: 1,
            pen1: 0,
            sd1: 0,
            score2: 2,
            pen2: 0,
            sd2: 0,
            team2: "FC Internazionale"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/02 13:10",
            team1: "Arranmore Utd",
            score1: 2,
            pen1: 0,
            sd1: 0,
            score2: 2,
            pen2: 0,
            sd2: 0,
            team2: "FC Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/02 13:35",
            team1: "Clannad Celtic",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 3,
            pen2: 0,
            sd2: 0,
            team2: "Northern Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/02 14:00",
            team1: "Tory Celtic",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 2,
            pen2: 0,
            sd2: 0,
            team2: "Stonecutters"
        },
        {
            type: "group",
            note: null,
            datetime: "2018/06/02 14:25",
            team1: "FC Shaktar",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 0,
            pen2: 0,
            sd2: 0,
            team2: "Luton Irish"
        },
        {
            note: "Quarter 1",
            type: "quarter",
            datetime: "2018/06/02 15:00",
            team1: "The Rebels",
            score1: 1,
            pen1: 1,
            sd1: 0,
            score2: 1,
            pen2: 2,
            sd2: 0,
            team2: "Damo's Dream"
        },
        {
            note: "Quarter 2",
            type: "quarter",
            datetime: "2018/06/02 15:30",
            team1: "Gallagher Tunnelling",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 1,
            pen2: 0,
            sd2: 0,
            team2: "Midland Warriors"
        },
        {
            note: "Quarter 3",
            type: "quarter",
            datetime: "2018/06/02 16:00",
            team1: "Stonecutters",
            score1: 4,
            pen1: 0,
            sd1: 0,
            score2: 0,
            pen2: 0,
            sd2: 0,
            team2: "Northern Tunnelling"
        },
        {
            note: "Quarter 4",
            type: "quarter",
            datetime: "2018/06/02 16:30",
            team1: "Tory Celtic",
            score1: 0,
            pen1: 0,
            sd1: 0,
            score2: 2,
            pen2: 0,
            sd2: 0,
            team2: "FC Palatico"
        },
        {
            note: "Semi 1",
            type: "semi",
            datetime: "2018/06/02 17:00",
            team1: "Stonecutters",
            score1: 1,
            pen1: 0,
            sd1: 0,
            score2: 3,
            pen2: 0,
            sd2: 0,
            team2: "Damo's Dream"
        },
        {
            note: "Semi 2",
            type: "semi",
            datetime: "2018/06/02 17:30",
            team1: "FC Palatico",
            score1: 1,
            pen1: 0,
            sd1: 0,
            score2: 0,
            pen2: 0,
            sd2: 0,
            team2: "Midland Warriors"
        },
        {
            note: "Fnial",
            type: "final",
            datetime: "2018/06/02 18:00",
            team1: "Damo's Dream",
            score1: 3,
            pen1: 0,
            sd1: 0,
            score2: 0,
            pen2: 0,
            sd2: 0,
            team2: "FC Palatico"
        }
    ],
    groups: [
        {
            group: "Group A",
            1: "Damo's Dream",
            2: "Midland Warriors",
            3: "Barhale",
            4: "Dungloe Town PLC"
        },
        {
            group: "Group B",
            1: "Gallagher Tunnelling",
            2: "Bayern Neverlusen",
            3: "FC Internationale",
            4: "The Rebels"
        },
        {
            group: "Group C",
            1: "Arranmore United",
            2: "Clannad Celtic",
            3: "Northern Tunnelling",
            4: "FC Palatico"
        },
        {
            group: "Group D",
            1: "Tory Celtic",
            2: "FC Shaktar",
            3: "Luton Irish",
            4: "Stonecutters"
        }
    ]
};

const results2019 = {
    year: "2019",
    type: "competition",
    results: [
        {
            _id: "5ce2d03f911883bd465946f5",
            type: "group",
            note: null,
            datetime: "2019/06/01, 11:30",
            team1: "Damo's Dream",
            score1: "1",
            pen1: "",
            sd1: "",
            team2: "Barhale",
            score2: "3",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d04b911883bd465946f9",
            type: "group",
            note: null,
            datetime: "2019/06/01, 11:55",
            team1: "FC Shaktar",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "FC Palatico",
            score2: "0",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d05d911883bd465946ff",
            type: "group",
            note: null,
            datetime: "2019/06/01, 12:20",
            team1: "Dungloe Town PLC",
            score1: "2",
            pen1: "",
            sd1: "",
            team2: "Tory Celtic",
            score2: "1",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d06c911883bd46594704",
            type: "group",
            note: null,
            datetime: "2019/06/01, 12:45",
            team1: "Clannad Celtic",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "Bayern Neverlusen",
            score2: "4",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d129911883bd4659473c",
            type: "group",
            note: null,
            datetime: "2019/06/01, 13:10",
            team1: "Luton Irish",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "FC Internaionale",
            score2: "1",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d135911883bd46594740",
            type: "group",
            note: null,
            datetime: "2019/06/01, 13:35",
            team1: "Midland Warriors",
            score1: "1",
            pen1: "",
            sd1: "",
            team2: "Arranmore United",
            score2: "0",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d13e911883bd46594744",
            type: "group",
            note: null,
            datetime: "2019/06/01, 14:00",
            team1: "Northern Tunnelling",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "Stonecutters",
            score2: "4",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d147911883bd46594748",
            type: "group",
            note: null,
            datetime: "2019/06/01, 14:30",
            team1: "The Rebels",
            score1: "",
            pen1: "",
            sd1: "",
            team2: "Gallagher Tunnelling",
            score2: "",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d1e0911883bd46594772",
            type: "group",
            note: null,
            datetime: "2019/06/01, 14:25",
            team1: "The Rebels",
            score1: "3",
            pen1: "",
            sd1: "",
            team2: "Gallagher Tunnelling",
            score2: "0",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d37d911883bd465947eb",
            type: "group",
            note: null,
            datetime: "2019/06/01, 14:50",
            team1: "Damo's Dream",
            score1: "1",
            pen1: "",
            sd1: "",
            team2: "FC Shaktar",
            score2: "0",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d389911883bd465947f0",
            type: "group",
            note: null,
            datetime: "2019/06/01, 15:15",
            team1: "Barhale",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "FC Palatico",
            score2: "1",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d394911883bd465947f4",
            type: "group",
            note: null,
            datetime: "2019/06/01, 15:40",
            team1: "Dungloe Town PLC",
            score1: "6",
            pen1: "",
            sd1: "",
            team2: "Clannad Celtic",
            score2: "0",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d3a0911883bd465947f8",
            type: "group",
            note: null,
            datetime: "2019/06/01, 16:05",
            team1: "Tory Celtic",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "Bayern Neverlusen",
            score2: "2",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d3ad911883bd465947fd",
            type: "group",
            note: null,
            datetime: "2019/06/01, 16:30",
            team1: "Luton Irish",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "Midland Warriors",
            score2: "2",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d3b6911883bd46594800",
            type: "group",
            note: null,
            datetime: "2019/06/01, 16:55",
            team1: "FC Internaionale",
            score1: "1",
            pen1: "",
            sd1: "",
            team2: "Arranmore United",
            score2: "2",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d3c3911883bd46594805",
            type: "group",
            note: null,
            datetime: "2019/06/01, 17:20",
            team1: "Northern Tunnelling",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "The Rebels",
            score2: "1",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5cf2b3dd8552376e493b5bcf",
            type: "group",
            note: null,
            datetime: "2019/06/01, 17:55",
            team1: "Stonecutters",
            score1: "2",
            pen1: "",
            sd1: "",
            team2: "Gallagher Tunnelling",
            score2: "0",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d3d9911883bd4659480d",
            type: "group",
            note: null,
            datetime: "2019/06/02, 11:30",
            team1: "Damo's Dream",
            score1: "1",
            pen1: "",
            sd1: "",
            team2: "FC Palatico",
            score2: "1",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d3e1911883bd46594811",
            type: "group",
            note: null,
            datetime: "2019/06/02, 11:55",
            team1: "Barhale",
            score1: "1",
            pen1: "",
            sd1: "",
            team2: "FC Shaktar",
            score2: "1",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d3ed911883bd46594815",
            type: "group",
            note: null,
            datetime: "2019/06/02, 12:20",
            team1: "Dungloe Town PLC",
            score1: "5",
            pen1: "",
            sd1: "",
            team2: "Bayern Neverlusen",
            score2: "1",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d3f8911883bd4659481a",
            type: "group",
            note: null,
            datetime: "2019/06/02, 12:45",
            team1: "Tory Celtic",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "Clannad Celtic",
            score2: "1",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d404911883bd4659481e",
            type: "group",
            note: null,
            datetime: "2019/06/02, 13:10",
            team1: "Luton Irish",
            score1: "3",
            pen1: "",
            sd1: "",
            team2: "Arranmore United",
            score2: "1",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d40c911883bd46594822",
            type: "group",
            note: null,
            datetime: "2019/06/02, 13:35",
            team1: "Midland Warriors",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "FC Internaionale",
            score2: "0",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d421911883bd46594828",
            type: "group",
            note: null,
            datetime: "2019/06/02, 14:00",
            team1: "Northern Tunnelling",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "Gallagher Tunnelling",
            score2: "0",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d43a911883bd4659482f",
            type: "group",
            note: null,
            datetime: "2019/06/02, 14:25",
            team1: "Stonecutters",
            score1: "0",
            pen1: "",
            sd1: "",
            team2: "The Rebels",
            score2: "1",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d469911883bd4659483c",
            type: "quarter",
            note: null,
            datetime: "Quarter 1",
            team1: "FC Palatico",
            score1: "0",
            pen1: "3",
            sd1: "",
            team2: "Bayern Neverlusen",
            score2: "0",
            pen2: "2",
            sd2: ""
        },
        {
            _id: "5ce2d482911883bd46594845",
            type: "quarter",
            note: null,
            datetime: "Quarter 2",
            team1: "The Rebels",
            score1: "0",
            pen1: "3",
            sd1: "",
            team2: "FC Internaionale",
            score2: "0",
            pen2: "2",
            sd2: ""
        },
        {
            _id: "5ce2d486911883bd46594848",
            type: "quarter",
            note: null,
            datetime: "Quarter 3",
            team1: "Dungloe Town PLC",
            score1: "2",
            pen1: "",
            sd1: "",
            team2: "Stonecutters",
            score2: "1",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d489911883bd4659484b",
            type: "quarter",
            note: null,
            datetime: "Quarter 4",
            team1: "Midland Warriors",
            score1: "2",
            pen1: "",
            sd1: "",
            team2: "Barhale",
            score2: "0",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d48e911883bd4659484e",
            type: "semi",
            note: null,
            datetime: "Semi 1",
            team1: "FC Palatico",
            score1: "1",
            pen1: "",
            sd1: "",
            team2: "Dungloe Town PLC",
            score2: "3",
            pen2: "",
            sd2: ""
        },
        {
            _id: "5ce2d491911883bd46594850",
            type: "semi",
            note: null,
            datetime: "Semi 2",
            team1: "Midland Warriors",
            score1: "1",
            pen1: "1",
            sd1: "",
            team2: "The Rebels",
            score2: "1",
            pen2: "2",
            sd2: ""
        },
        {
            _id: "5ce2d496911883bd46594853",
            type: "final",
            note: null,
            datetime: "Final",
            team1: "Dungloe Town PLC",
            score1: "1",
            pen1: "2",
            sd1: "",
            team2: "The Rebels",
            score2: "1",
            pen2: "1",
            sd2: ""
        }
    ],
    groups: [
        {
            group: "Group A",
            1: "Damo's Dream",
            2: "Barhale",
            3: "FC Shaktar",
            4: "FC Palatico"
        },
        {
            group: "Group B",
            1: "Dungloe",
            2: "Tory Celtic",
            3: "Clannad Celtic",
            4: "Bayern Neverlusen"
        },
        {
            group: "Group C",
            1: "Luton Irish",
            2: "FC Internationale",
            3: "Midland Warriors",
            4: "Arranmore United"
        },
        {
            group: "Group D",
            1: "Northern Tunnelling",
            2: "Stonecutters",
            3: "The Rebels",
            4: "Gallagher Tunnelling"
        }
    ]
};

const results2017 = {
    year: "2017",
    type: "competition",
    results: [
        {
            type: "group",
            note: null,
            datetime: "Day 1, 11:30",
            team1: "Real Lypis",
            score1: "2",
            score2: "1",
            team2: "Dungloe Town PLC"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 11:55",
            team1: "M.T.F.C.",
            score1: "3",
            score2: "0",
            team2: "Northern Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 12:20",
            team1: "Midland Warriors",
            score1: "2",
            score2: "2",
            team2: "The Saturdays"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 12:45",
            team1: "FC Palatico",
            score1: "2",
            score2: "0",
            team2: "Arranmore Utd"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 13:10",
            team1: "Tory Celtic",
            score1: "0",
            score2: "0",
            team2: "Gallagher Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 13:35",
            team1: "Clannad Celtic",
            score1: "0",
            score2: "2",
            team2: "Stonecutters"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:00",
            team1: "The Rebels",
            score1: "0",
            score2: "0",
            team2: "Damo's Dream"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:30",
            team1: "Real Lypis",
            score1: "3",
            score2: "0",
            team2: "M.T.F.C."
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:55",
            team1: "Dungloe Town PLC",
            score1: "1",
            score2: "1",
            team2: "Northern Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 15:20",
            team1: "Midland Warriors",
            score1: "0",
            score2: "1",
            team2: "FC Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 15:45",
            team1: "The Saturdays",
            score1: "2",
            score2: "0",
            team2: "Arranmore Utd"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 16:10",
            team1: "Tory Celtic",
            score1: "1",
            score2: "0",
            team2: "Clannad Celtic"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 16:35",
            team1: "Gallagher Tunnelling",
            score1: "1",
            score2: "0",
            team2: "Stonecutters"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 17:00",
            team1: "Damo's Dream",
            score1: "3",
            score2: "1",
            team2: "The Rebels"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 12:00",
            team1: "Real Lypis",
            score1: "5",
            score2: "0",
            team2: "Northern Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 12:25",
            team1: "Dungloe Town PLC",
            score1: "2",
            score2: "0",
            team2: "M.T.F.C."
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 12:50",
            team1: "Midland Warriors",
            score1: "0",
            score2: "0",
            team2: "Arranmore Utd"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 13:15",
            team1: "The Saturdays",
            score1: "-",
            score2: "-",
            team2: "FC Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 13:50",
            team1: "Tory Celtic",
            score1: "-",
            score2: "-",
            team2: "Stonecutters"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 14:15",
            team1: "Gallagher Tunnelling",
            score1: "4",
            score2: "2",
            team2: "Clannad Celtic"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 14:40",
            team1: "The Rebels",
            score1: "2",
            score2: "0",
            team2: "Barhale"
        },
        {
            type: "quarter",
            datetime: "Quarter 1",
            team1: "Damo's Dream",
            score1: "3",
            score2: "0",
            team2: "The Saturdays"
        },
        {
            type: "quarter",
            datetime: "Quarter 2",
            team1: "The Rebels",
            score1: "0",
            pen1: 1,
            pen2: 0,
            score2: "0",
            team2: "FC Palatico"
        },
        {
            type: "quarter",
            datetime: "Quarter 3",
            team1: "Real Lypis",
            score1: "3",
            score2: "0",
            team2: "Tory Celtic"
        },
        {
            type: "quarter",
            datetime: "Quarter 4",
            team1: "Gallagher Tunnelling",
            score1: "1",
            score2: "0",
            team2: "Dungloe Town PLC"
        },
        {
            type: "semi",
            datetime: "Semi 1",
            team1: "Damo's Dream",
            score1: "1",
            score2: "0",
            team2: "Real Lypis"
        },
        {
            type: "semi",
            datetime: "Semi 2",
            team1: "Gallagher Tunnelling",
            score1: "0",
            pen1: 2,
            sd1: 1,
            pen2: 2,
            sd2: 0,
            score2: "0",
            team2: "The Rebels"
        },
        {
            type: "final",
            datetime: "FINAL",
            team1: "Gallagher Tunnelling",
            score1: 0,
            score2: 1,
            team2: "Damo's Dream"
        }
    ],
    groups: [
        {
            1: "Real Lypis",
            2: "Dungloe Town PLC",
            3: "M.T.F.C.",
            4: "Northern Tunnelling",
            group: "groupA"
        },
        {
            1: "Midland Warriors",
            2: "The Saturdays",
            3: "FC Palatico",
            4: "Arranmore Utd",
            group: "groupB"
        },
        {
            1: "Tory Celtic",
            2: "Gallagher Tunnelling",
            3: "Clannad Celtic",
            4: "Stonecutters",
            group: "groupC"
        },
        {
            1: "The Rebels",
            2: "Damo's Dream",
            3: "-",
            4: "Barhale",
            group: "groupD"
        }
    ]
};

const results2016 = {
    year: "2016",
    type: "competition",
    group: [
        {
            1: "Real Lypis",
            2: "Dungloe Town PLC",
            3: "M.T.F.C.",
            4: "Northern Tunnelling",
            group: "groupA"
        },
        {
            1: "Midland Warriors",
            2: "The Saturdays",
            3: "FC Palatico",
            4: "Arranmore Utd",
            group: "groupB"
        },
        {
            1: "Tory Celtic",
            2: "Gallagher Tunnelling",
            3: "Clannad Celtic",
            4: "Stonecutters",
            group: "groupC"
        },
        {
            1: "The Rebels",
            2: "Damo's Dream",
            3: "-",
            4: "Barhale",
            group: "groupD"
        }
    ],
    results: [
        {
            type: "group",
            note: null,
            datetime: "Day 1, 11:30",
            team1: "Damo's Dream",
            score1: "2",
            score2: "1",
            team2: "Stonecutters"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 11:55",
            team1: "Northern Tunnelling",
            score1: "0",
            score2: "1",
            team2: "Finn Harps Youths"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 12:20",
            team1: "Clannad Celtic",
            score1: "0",
            score2: "2",
            team2: "Luton Irish"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 12:45",
            team1: "FC Palatico",
            score1: "1",
            score2: "0",
            team2: "Barhale"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 13:10",
            team1: "St Anne's United",
            score1: "0",
            score2: "2",
            team2: "Gallagher Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 13:35",
            team1: "Midland Warriors",
            score1: "0",
            score2: "3",
            team2: "Real Lypis"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:00",
            team1: "The Saturdays",
            score1: "0",
            score2: "2",
            team2: "The Rebels"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:25",
            team1: "Tory Select",
            score1: "1",
            score2: "0",
            team2: "Arranmore Select"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:50",
            team1: "Damo's Dream",
            score1: "0",
            score2: "0",
            team2: "Northern Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 15:15",
            team1: "Stonecutters",
            score1: "1",
            score2: "2",
            team2: "Finn Harps Youths"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 15:40",
            team1: "Clannad Celtic",
            score1: "0",
            score2: "4",
            team2: "FC Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 16:05",
            team1: "Luton Irish",
            score1: "2",
            score2: "0",
            team2: "Barhale"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 16:30",
            team1: "St Anne's United",
            score1: "0",
            score2: "2",
            team2: "Midland Warriors"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 16:55",
            team1: "Gallagher Tunnelling",
            score1: "0",
            score2: "1",
            team2: "Real Lypis"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 17:20",
            team1: "The Saturdays",
            score1: "0",
            score2: "1",
            team2: "Arranmore Select"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 17:45",
            team1: "The Rebels",
            score1: "1",
            score2: "0",
            team2: "Tory Select"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 11:30",
            team1: "Damo's Dream",
            score1: "1",
            score2: "0",
            team2: "Finn Harps Youths"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 11:55",
            team1: "Stonecutters",
            score1: "1",
            score2: "0",
            team2: "Northern Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 12:20",
            team1: "Clannad Celtic",
            score1: "1",
            score2: "5",
            team2: "Barhale"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 12:45",
            team1: "Luton Irish",
            score1: "1",
            score2: "3",
            team2: "FC Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 13:10",
            team1: "St Anne's United",
            score1: "0",
            score2: "2",
            team2: "Real Lypis"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 13:35",
            team1: "Gallagher Tunnelling",
            score1: "1",
            score2: "0",
            team2: "Midland Warriors"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 14:00",
            team1: "The Saturdays",
            score1: "1",
            score2: "3",
            team2: "Tory Select"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 14:25",
            team1: "The Rebels",
            score1: "0",
            score2: "2",
            team2: "Arranmore Select"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 1",
            team1: "Damo's Dream",
            score1: "2",
            score2: "1",
            team2: "Gallagher Tunnelling"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 2",
            team1: "FC Palatico",
            score1: "3",
            score2: "0",
            team2: "The Rebels"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 3",
            team1: "Real Lypis",
            score1: "2",
            score2: "0",
            team2: "Luton Irish"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 4",
            team1: "Finn Harps Youths",
            score1: "0",
            score2: "2",
            team2: "Tory Select"
        },
        {
            type: "semi",
            note: null,
            datetime: "Semi 1",
            team1: "Damo's Dream",
            score1: 1,
            pen1: 1,
            score2: 1,
            pen2: 2,
            team2: "FC Palatico"
        },
        {
            type: "semi",
            note: null,
            datetime: "Semi 2",
            team1: "Tory Select",
            score1: "1",
            score2: "3",
            team2: "Real Lypis"
        },
        {
            type: "final",
            note: null,
            datetime: "FINAL",
            team1: "FC Palatico",
            score1: "0",
            score2: "1",
            team2: "Real Lypis"
        }
    ]
};

const results2015 = {
    year: "2015",
    type: "competition",
    groups: [
        {
            1: "Real Lypis",
            2: "Dungloe Town PLC",
            3: "M.T.F.C.",
            4: "Northern Tunnelling",
            group: "groupA"
        },
        {
            1: "Midland Warriors",
            2: "The Saturdays",
            3: "FC Palatico",
            4: "Arranmore Utd",
            group: "groupB"
        },
        {
            1: "Tory Celtic",
            2: "Gallagher Tunnelling",
            3: "Clannad Celtic",
            4: "Stonecutters",
            group: "groupC"
        },
        {
            1: "The Rebels",
            2: "Damo's Dream",
            3: "-",
            4: "Barhale",
            group: "groupD"
        }
    ],
    results: [
        {
            type: "group",
            note: null,
            datetime: "Day 1, 11:30",
            team1: "Finn Harps Youths",
            score1: "2",
            score2: "1",
            team2: "Tory Celtic"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 11:55",
            team1: "Stonecutters",
            score1: "1",
            score2: "0",
            team2: "Barhale"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 12:20",
            team1: "The Rebels",
            score1: "0",
            score2: "2",
            team2: "F.C. Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 12:45",
            team1: "Riverside F.C.",
            score1: "0",
            score2: "0",
            team2: "Arranmore Utd"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 13:10",
            team1: "Midland Warriors",
            score1: "0",
            score2: "0",
            team2: "FC Internazionale"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 13:35",
            team1: "Real Lypis",
            score1: "3",
            score2: "0",
            team2: "Gallagher Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:00",
            team1: "St. Annes",
            score1: "0",
            score2: "5",
            team2: "Damo's Dream"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:25",
            team1: "Northern Tunnelling",
            score1: "0",
            score2: "4",
            team2: "Clannad Celtic"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:50",
            team1: "Finn Harps Youths",
            score1: "0",
            score2: "1",
            team2: "Stonecutters"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 15:15",
            team1: "The Rebels",
            score1: "2",
            score2: "0",
            team2: "Riverside F.C."
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 15:40",
            team1: "Tory Celtic",
            score1: "0",
            score2: "2",
            team2: "Barhale"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 16:05",
            team1: "F.C. Palatico",
            score1: "2",
            score2: "0",
            team2: "Arranmore Utd"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 16:30",
            team1: "Midland Warriors",
            score1: "0",
            score2: "0",
            team2: "Real Lypis"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 16:55",
            team1: "FC Internazionale",
            score1: "1",
            score2: "0",
            team2: "Gallagher Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 17:20",
            team1: "St. Annes",
            score1: "2",
            score2: "1",
            team2: "Northern Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 17:45",
            team1: "Damo's Dream",
            score1: "4",
            score2: "0",
            team2: "Clannad Celtic"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 11:30",
            team1: "Finn Harps Youths",
            score1: "1",
            score2: "1",
            team2: "Barhale"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 11:55",
            team1: "Stonecutters",
            score1: "2",
            score2: "0",
            team2: "Tory Celtic"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 12:20",
            team1: "The Rebels",
            score1: "2",
            score2: "0",
            team2: "Arranmore Utd"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 12:45",
            team1: "F.C. Palatico",
            score1: "2",
            score2: "0",
            team2: "Riverside F.C."
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 13:10",
            team1: "Midland Warriors",
            score1: "3",
            score2: "0",
            team2: "Gallagher Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 13:35",
            team1: "FC Internazionale",
            score1: "1",
            score2: "1",
            team2: "Real Lypis"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 14:00",
            team1: "St. Annes",
            score1: "1",
            score2: "5",
            team2: "Clannad Celtic"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 14:25",
            team1: "Damo's Dream",
            score1: "4",
            score2: "0",
            team2: "Northern Tunnelling"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 1",
            team1: "Clannad Celtic",
            score1: "2",
            score2: "0",
            team2: "Stonecutters"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 2",
            team1: "Damo's Dream",
            score1: "1",
            score2: "0",
            team2: "Barhale"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 3",
            team1: "Midland Warriors",
            score1: "0",
            score2: "1",
            team2: "F.C. Palatico"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 4",
            team1: "The Rebels",
            score1: "0",
            score2: "3",
            team2: "Real Lypis"
        },
        {
            type: "semi",
            note: null,
            datetime: "Semi 1",
            team1: "Damo's Dream",
            score1: 2,
            pen1: 3,
            score2: 2,
            pen2: 2,
            team2: "F.C. Palatico"
        },
        {
            type: "semi",
            note: null,
            datetime: "Semi 2",
            team1: "Clannad Celtic",
            score1: "0",
            score2: "3",
            team2: "Real Lypis"
        },
        {
            type: "final",
            note: null,
            datetime: "FINAL",
            team1: "Damo's Dream",
            score1: 1,
            pen1: 3,
            sd1: 1,
            score2: 1,
            pen2: 3,
            sd2: 0,
            team2: "Real Lypis"
        }
    ]
};

const results2022 = {
    year: "2022",
    type: "competition",
    groups: [
        {
            1: "Dungloe Town PLC",
            2: "Tory Celtic",
            3: "Bayern Neverlusen",
            4: "Zoo FC",
            group: "Group A"
        },
        {
            1: "FC Shaktar",
            2: "Stonecutters",
            3: "Clannad Celtic",
            4: "The All Backs",
            group: "Group B"
        },
        {
            1: "Northern Tunnelling",
            2: "Danbury Trashers",
            3: "Arranmore United",
            4: "The Rebels",
            group: "Group C"
        },
        {
            1: "Damo's Dream",
            2: "The Saturdays",
            3: "FC Palatico",
            4: "Midland Warriors",
            group: "Group D"
        }
    ],
    results: [
        {
            type: "group",
            note: null,
            datetime: "Day 1, 12:00",
            team1: "Bayern Neverlusen",
            score1: "2",
            score2: "0",
            team2: "Zoo FC"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 12:25",
            team1: "FC Shaktar",
            score1: "0",
            score2: "2",
            team2: "Stonecutters"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 12:50",
            team1: "Clannad Celtic",
            score1: "0",
            score2: "3",
            team2: "The All Backs"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 13:15",
            team1: "Nothern Tunnelling",
            score1: "1",
            score2: "2",
            team2: "Danbury Trashers"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 13:40",
            team1: "Arranmore United",
            score1: "1",
            score2: "0",
            team2: "The Rebels"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:05",
            team1: "Damo's Dream",
            score1: "5",
            score2: "0",
            team2: "FC Internazionale"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:30",
            team1: "FC Palatico",
            score1: "0",
            score2: "3",
            team2: "Midland Warriors"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 14:55",
            team1: "Dungloe Town",
            score1: "1",
            score2: "1",
            team2: "Bayern Neverlusen"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 15:20",
            team1: "FC Shaktar",
            score1: "2",
            score2: "1",
            team2: "Clannad Celtic"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 15:45",
            team1: "Stonecutters",
            score1: "0",
            score2: "1",
            team2: "The All Backs"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 16:10",
            team1: "Northern Tunnelling",
            score1: "0",
            score2: "3",
            team2: "Arranmore United"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 16:35",
            team1: "Danbury Trashers",
            score1: "1",
            score2: "1",
            team2: "The Rebels"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 17:00",
            team1: "Damo's Dream",
            score1: "3",
            score2: "0",
            team2: "FC Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 1, 17:25",
            team1: "FC Internazionale",
            score1: "1",
            score2: "3",
            team2: "Midland Warrios"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 12:00",
            team1: "Dungloe Town",
            score1: "-",
            score2: "-",
            team2: "Zoo FC"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 12:25",
            team1: "FC Shaktar",
            score1: "-",
            score2: "-",
            team2: "The All Backs"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 12:50",
            team1: "Stonecutters",
            score1: "-",
            score2: "-",
            team2: "Clannad Celtic"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 13:15",
            team1: "Northern Tunnelling",
            score1: "-",
            score2: "-",
            team2: "The Rebels"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 13:40",
            team1: "Danbury Trashers",
            score1: "-",
            score2: "-",
            team2: "Arranmore United"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 14:05",
            team1: "Damo's Dream",
            score1: "-",
            score2: "-",
            team2: "Midland Warriors"
        },
        {
            type: "group",
            note: null,
            datetime: "Day 2, 14:30",
            team1: "FC Internazionale",
            score1: "-",
            score2: "-",
            team2: "FC Palatico"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 1",
            team1: "",
            score1: "-",
            score2: "-",
            team2: "-"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 2",
            team1: "",
            score1: "-",
            score2: "-",
            team2: "-"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 3",
            team1: "",
            score1: "-",
            score2: "-",
            team2: "-"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 4",
            team1: "",
            score1: "-",
            score2: "-",
            team2: "-"
        },
        {
            type: "semi",
            note: null,
            datetime: "Semi 1",
            team1: "",
            score1: "-",
            score2: "-",
            team2: "-"
        },
        {
            type: "semi",
            note: null,
            datetime: "Semi 2",
            team1: "",
            score1: "-",
            score2: "-",
            team2: "-"
        },
        {
            type: "final",
            note: null,
            datetime: "FINAL",
            team1: "Arranmore Utd",
            score1: "-",
            score2: "-",
            team2: "Damo's Dream"
        }
    ]
};

const results2023 = {
    year: "2023",
    type: "competition",
    groups: [
        {
            1: "Damo's Dream",
            2: "Dungloe Town",
            3: "The All Backs",
            4: "Arranmore United",
            group: "Group A"
        },
        {
            1: "Danbury Trashers",
            2: "Fintown Harps",
            3: "Bayern Neverlusen",
            4: "Northern Tunnelling",
            group: "Group B"
        },
        {
            1: "Koala Cubs",
            2: "FC Shaktar",
            3: "Stonecutters",
            4: "FC Palatico",
            group: "Group C"
        },
        {
            1: "Zoo FC",
            2: "Clannad Celtic",
            3: "Tobár Mór Aontaithe",
            4: "Midland Warriors",
            group: "Group D"
        }
    ],
    results: [
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 12.00 PM",
            team1: "The All Backs",
            score1: "3",
            score2: "0",
            team2: "Arranmore United"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 12.25 PM",
            team1: "Danbury Trashers",
            score1: "1",
            score2: "0",
            team2: "Fintown"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 12.50 PM",
            team1: "Bayern Neverlusen",
            score1: "3",
            score2: "1",
            team2: "Northern Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 1.15 PM",
            team1: "Koala Cubs",
            score1: "3",
            score2: "0",
            team2: "FC Shaktar"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 1.40 PM",
            team1: "Stonecutters",
            score1: "2",
            score2: "1",
            team2: "FC Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 2.05 PM",
            team1: "Zoo FC",
            score1: "8",
            score2: "0",
            team2: "Clannad Celtic"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 2.30 PM",
            team1: "Tóbar Mor Aontaithe",
            score1: "0",
            score2: "6",
            team2: "Midland Warriors"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 2.55 PM",
            team1: "Damo's Dream",
            score1: "0",
            score2: "2",
            team2: "The All Backs"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 3.20 PM",
            team1: "Danbury Trashers",
            score1: "2",
            score2: "0",
            team2: "Bayern Neverlusen"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 3.45 PM",
            team1: "Fintown",
            score1: "2",
            score2: "0",
            team2: "Northern Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 4.10 PM",
            team1: "Koala Cubs",
            score1: "0",
            score2: "2",
            team2: "Stonecutters"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 4.35 PM",
            team1: "FC Shaktar",
            score1: "0",
            score2: "2",
            team2: "FC Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 5.00 PM",
            team1: "Zoo FC",
            score1: "6",
            score2: "0",
            team2: "Tóbar Mor Aontaithe"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 1, 5.25 PM",
            team1: "Clannad Celtic",
            score1: "0",
            score2: "5",
            team2: "Midland Warriors"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 2, 12.00 PM",
            team1: "Damo's Dream",
            score1: "2",
            score2: "1",
            team2: "Arranmore United"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 2, 12.25 PM",
            team1: "Danbury Trashers",
            score1: "4",
            score2: "0",
            team2: "Northern Tunnelling"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 2, 12.50 PM",
            team1: "Fintown",
            score1: "0",
            score2: "1",
            team2: "Bayern Neverlusen"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 2, 1.15 PM",
            team1: "Koala Cubs",
            score1: "5",
            score2: "4",
            team2: "FC Palatico"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 2, 1.40 PM",
            team1: "FC Shaktar",
            score1: "2",
            score2: "3",
            team2: "Stonecutters"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 2, 2.05 PM",
            team1: "Zoo FC",
            score1: "1",
            score2: "1",
            team2: "Midland Warriors"
        },
        {
            type: "group",
            note: null,
            datetime: "DAY 2, 2.30 PM",
            team1: "Clannad Celtic",
            score1: "2",
            score2: "5",
            team2: "Tóbar Mor Aontaithe"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 1, 3.10 PM",
            team1: "All Backs",
            score1: "5",
            score2: "0",
            team2: "Koala Cubs"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 2, 3.25 PM",
            team1: "Zoo FC",
            score1: "2",
            score2: "1",
            team2: "Bayern Neverlusen"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 3, 3.50 PM",
            team1: "Danbury Trashers",
            score1: "3",
            score2: "0",
            team2: "Warriors"
        },
        {
            type: "quarter",
            note: null,
            datetime: "Quarter 4, 4.15 PM",
            team1: "Stonecutters",
            score1: "2",
            score2: "1",
            team2: "Damo's Dream"
        },
        {
            type: "semi",
            note: null,
            datetime: "Semi 1, 4.40 PM",
            team1: "All Backs",
            score1: "1",
            score2: "0",
            team2: "Danbury Trashers"
        },
        {
            type: "semi",
            note: null,
            datetime: "Semi 2, 5.05 PM",
            team1: "Stonecutters",
            score1: "-",
            score2: "WON",
            team2: "Zoo FC"
        },
        {
            type: "final",
            note: null,
            datetime: "FINAL, 6.30 PM",
            team1: "All Blacks",
            score1: "5",
            score2: "1",
            team2: "Zoo FC"
        }
    ]
};

// RESULTS
export const results = [
    results2023,
    results2022,
    results2019,
    results2018,
    results2017,
    results2016,
    results2015,
    results2014,
    results2013
];
