import Head from 'next/head'
import Link from 'next/link'
// import styles from '../styles/Home.module.css'

import React from 'react';

// import { getSortedPostsData } from '../lib/posts'
import {
	Stack,
	Container,
	Heading,
	Box,
	Table,
	Thead,
	Tbody,
	Tfoot,
	Button,
	Tr,
	Th,
	Td,
	TableCaption,
	Text,
	Divider,
	Wrap,
	WrapItem,
	useColorModeValue
} from '@chakra-ui/react'
import NavBar from '../../components/common/navbar';
import Footer from '../../components/common/footer';

// import { InfoIcon } from '@chakra-ui/icons';

import { getArchivedYears, getHistoricalWinners } from '../../lib/results'


export function getStaticProps() {
	const historicalWinners = getHistoricalWinners();
	const archivedYears = getArchivedYears();
	// console.log("archivedYears");
	// console.log(archivedYears);
	// const sortedYears = 
	return {
		props: {
			historicalWinners,
			archivedYears
		}
	}
}

export default function About({ historicalWinners, archivedYears }) {
	return (
		<>
			<Head>
				<title>Arranmore Challenge | Results from Arranmore Island's Football Competition</title>
				<link rel="icon" href="/favicon.ico" />
			</Head>
			<NavBar />
			<Box p={0}>
				<Stack spacing={4} as={Container} mb={0} maxW={'3xl'} textAlign={'left'}>
					<Heading mt={10} size={'xl'}>Results</Heading>
					<Text>There are no competitions on right now - browse the archives for previous results</Text>
					<Divider />
					<Heading mt={10} size={'md'}>Results from Past Challenges</Heading>
					{/* <Text>There are no competitions on right now - browse the archives for previous results</Text> */}
					<Wrap>{archivedYears.map((e) =>
						<WrapItem key={e.year}>
							<Link href={`/results/${e.year}`}>
								<Button color={"white"} bg={useColorModeValue('primary.400', 'primary.400')} _hover={{ bg: useColorModeValue('primary.100', 'primary.100') }}>
									{e.year}
								</Button>
							</Link>
						</WrapItem>)}
					</Wrap>
					<Divider />
					{/* <Text>{"TEST:" + JSON.stringify(archivedYears)}</Text> */}
				</Stack>

				<Stack spacing={4} as={Container} mb={10} maxW={'4xl'} textAlign={'left'}>
					<Heading mt={10} size={'lg'}>History of Champions</Heading>
					<Table variant="striped">
						<TableCaption>Winners of the past Arranmore Challenge</TableCaption>
						<Thead>
							<Tr>
								<Th>Year</Th>
								<Th>Winners</Th>
								<Th>Runners Up</Th>
							</Tr>
						</Thead>
						<Tbody>
							{historicalWinners.map((e) => <Tr>
								<Td>{e.year}</Td>
								<Td><stong>{e.winners}</stong></Td>
								<Td>{e.runners}</Td>
							</Tr>)}
						</Tbody>
						<Tfoot>
							<Tr>
								<Th>Year</Th>
								<Th>Winners</Th>
								<Th>Runners Up</Th>
							</Tr>
						</Tfoot>
					</Table>
				</Stack>
			</Box>
			<Footer />
		</>
	)
}
