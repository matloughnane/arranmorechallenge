# UPLOAD SCRIPT
# CHOOSE ENVIRONMENT
echo "Which environment are you uploading to:"
echo "1. budweiser Production"
echo "2. aff.hexa.design"
echo "3. Other"

read uploadenv

if [ "$uploadenv" = "1" ]; then
	# UPLOAD SCRIPTS
	addr='root@budweiser:/var/www/arranmorechallenge.com/public_html'
elif [ "$uploadenv" = "2" ]; then
	addr='root@budweiser:/var/www/aff.hexa.design/public_html'
fi

echo "Have you made backups?"
echo "Y | N"

read confirmbkp

if [ $confirmbkp = "Y" ] || [ $confirmbkp = "y"  ] || [ $confirmbkp = "yes"  ]; then
	# UPLOAD SCRIPTS
	echo "Making upload"
	scp -r ../dist/* $addr
	scp ../assets/css/styles.css $addr/assets/css/
	scp ../assets/css/styles.css.map $addr/assets/css/
	scp ../assets/js/*.min.js $addr/assets/js/
	scp ../assets/js/*.min.js.map $addr/assets/js/
	scp -r ../assets/img/* $addr/assets/img/
	scp ../assets/font/* $addr/assets/font/
	scp ../*.js $addr/
	# ROOT FILES
	scp ./root-files/* $addr
	echo "Upload complete"
else
	echo "Make a backup now, idiot!"
fi


# # UPLOAD HTML
# scp ../dist/* 