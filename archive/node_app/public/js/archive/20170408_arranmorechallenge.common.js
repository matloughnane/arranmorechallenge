// COMMON FUNCTIONS

var endpoint = "http://localhost:2017";
// var endpoint = "https://live.arranmorechallenge.com";
var refreshRate = 60*1000;


// setInterval(function() {
//     // your code goes here...
// }, 60 * 1000);

loadMessages();

function loadMessages(){
	$.get( endpoint+"/updates", function( data ) {
		// console.log("initial load");
		// console.log(data);
		if(data.messages.length == 0 ){
			$('#messagesContainer').html('<div class="align-center">There are no messages at this time.</div>');
		} else {
			// do nothing yet
			for (var i = data.messages.length - 1; i >= 0; i--) {
				// console.log(data.messages[i]);
				if (data.messages[i].adminStatus == "true"){
					var adminMessageHTML = constructAdminMessage(data.messages[i])
					/// prepend to messages
					$("#messagesContainer").prepend(adminMessageHTML);
				} else {
					var fanMessageHTML = constructFanMessage(data.messages[i])
					$("#messagesContainer").prepend(fanMessageHTML);
				}
			}

			// HIDE THE LATEST MESSAGE ID
			$('#latestMessageTime').val(data.messages[0].timestamp);

		}
	});
};

function constructAdminMessage(messageObj){

	var html = '<div class="unit w-2-3 card plain card-message admin">';
	html += '<div class="profile-icon align-center">';
	if (messageObj.userProfileImage){
		html += '<img src="'+messageObj.userProfileImage+'" />';
	}

	var timestamp = getTimeString(messageObj.timestamp);

	html +='</div><div class="message-text-admin">';
	html += '<h3 class="username">'+messageObj.username+'</h3><span class="message-timestamp">'+timestamp+'</span><br>';
	html += '<span class="status">'+messageObj.content+'</span>';
	html += '</div></div>';

	return html

    // <div class="unit w-2-3 card plain card-message admin" >
    //     <div class="profile-icon"></div>
    //     <div class="message-text-admin">
    //         <h3 class="username">arranchallenge</h3> <br>
    //         <span class="status">This is the new format for the 2017 live Challenge update</span>
    //     </div>
    // </div>
};

function constructFanMessage(messageObj){

	var html = '<div class="unit w-2-3 card plain card-message fan">';
	html += '<div class="profile-icon-fan" id="profile_'+messageObj.userID+'">';
	// if (messageObj.userProfileImage){
	// 	html += '<img class="twitterImage" src="'+messageObj.userProfileImage+'" />';
	// }

	console.log(messageObj.timestamp);

	var timestamp = getTimeString(messageObj.timestamp);

	html +='</div><div class="message-text-fan">';
	html += '<h3 class="username">'+messageObj.username+'</h3><span class="message-timestamp">'+timestamp+'</span><br>';
	html += '<span class="status">'+messageObj.content+'</span>';
	html += '</div></div>';
	return html

    // <div class="unit w-2-3 card plain card-message fan" >
    //     <div class="profile-icon-fan"></div>
    //     <div class="message-text-fan">
    //         <a href="/profile/{id}"><h3 class="username">mat.loughnane</h3></a> <br>
    //         <span class="status">This is the new format for the 2017 live Challenge update</span>
    //     </div>
    //     <div class="message-share-fan">
    //         <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//live.arranmorechallenge.com/status/{id}" target="_blank" class="message-share-button"><i class="icon-facebook-official"></i></a> 
    //         <a href="https://twitter.com/home?status=http%3A//live.arranmorechallenge.com/status/{id}" target="_blank" class="message-share-button"><i class="icon-twitter"></i></a> 
    //         <a href="/status/{id}" target="_blank" class="message-share-button"><i class="icon-link"></i></a>
    //         <span class="message-timestamp">11:07am 15/01</span>
    //     </div> 
    // </div>
};

function getTimeString(epoch){
	var date = new Date(parseInt(epoch));
	// console.log(date);
	var hours = lessThanTen(date.getHours());
	var minutes = lessThanTen(date.getMinutes());

	var stringDate = lessThanTen(date.getDate());
	var stringMonth = lessThanTen(date.getMonth()+1);

	var string = hours+":"+minutes+" "+stringDate+"/"+stringMonth;
	return string;
}

function lessThanTen(number){
	if (number < 10){
		return "0"+number
	} else {
		return number
	}
}

$('#checkUpdates').click(function(){
	// console.log("check updates");
	// GET LATEST
	getLatestMessages();
});


function getLatestMessages(){
	var myCurrentTime = $('#latestMessageTime').val();

	if (typeof myCurrentTime == "string") {
		myCurrentTime = 0
	}

	$.get( endpoint+"/updates/latest/"+myCurrentTime, function( data ) {
		// console.log("initial load");
		console.log(data);
		if(data.messages.length == 0 ){
			// $('#messagesContainer').html('<div class="align-center">There are no messages at this time.</div>');
		} else {
			// CLEAR MESSAGES
			$('#messagesContainer').html('');
			// do nothing yet
			for (var i = data.messages.length - 1; i >= 0; i--) {
				// console.log(data.messages[i]);
				if (data.messages[i].adminStatus == "true"){
					var adminMessageHTML = constructAdminMessage(data.messages[i])
					/// prepend to messages
					$("#messagesContainer").prepend(adminMessageHTML);
				} else {
					var fanMessageHTML = constructFanMessage(data.messages[i])
					$("#messagesContainer").prepend(fanMessageHTML);
				}
			}

			// HIDE THE LATEST MESSAGE ID
			if (data.messages.length > 0){
				$('#latestMessageTime').val(data.messages[0].timestamp);				
			}

		}
	});
};