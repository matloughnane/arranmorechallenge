// import { ReactNode } from 'react';
import {
	Box,
	Flex,
	// Avatar,
	HStack,
	IconButton,
	Heading,
	Button,
	// Menu,
	// MenuButton,
	// MenuList,
	// MenuItem,
	// MenuDivider,
	useDisclosure,
	useColorModeValue,
	useColorMode,
	Stack,
} from '@chakra-ui/react';

import { HamburgerIcon, CloseIcon } from '@chakra-ui/icons';
import { FaMoon, FaSun } from 'react-icons/fa';

import Link from 'next/link';

const Links = [
	'About',
	'Results',
	//  'Live',
	//  'App'
];

const NavLink = ({ children }) => (
	<Link
		href={"/" + children.toLowerCase()}>
		<Button px={2}
			py={1}
			rounded={'md'}
			color={"white"}
			bg={useColorModeValue('primary.400', 'primary.900')}
			_hover={{
				textDecoration: 'none',
				// bg: useColorModeValue('primary.100', 'primary.100'),
				bg: useColorModeValue('primary.100', 'primary.400')
			}}>
			{children}
		</Button>
	</Link>
);

export default function NavBar() {
	const { isOpen, onOpen, onClose } = useDisclosure();

	return (
		<>
			<Box bg={useColorModeValue('primary.400', 'primary.900')} px={4}  >
				<Flex h={16} alignItems={'center'} justifyContent={'space-between'} style={{ margin: "0 auto" }} maxW="1200px">
					<HStack spacing={8} alignItems={'center'}>
						<Box>
							<Link href="/">
								<Heading size="md" color={"white"} style={{ cursor: "pointer" }}>
									Arranmore Challenge
								</Heading>
							</Link>
						</Box>
					</HStack>
					<HStack
						as={'nav'}
						spacing={4}
						color={"white"}
						display={{ base: 'none', md: 'flex' }}>
						{Links.map((link) => (
							<NavLink key={link}>{link}</NavLink>
						))}
						<ThemeToggle />
					</HStack>
					{/* <Flex alignItems={'center'}>
						<Menu>
							<MenuButton
								as={Button}
								rounded={'full'}
								variant={'link'}
								cursor={'pointer'}>
								<Avatar
									size={'sm'}
									src={
										'https://images.unsplash.com/photo-1493666438817-866a91353ca9?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&s=b616b2c5b373a80ffc9636ba24f7a4a9'
									}
								/>
							</MenuButton>
							<MenuList>
								<MenuItem>Link 1</MenuItem>
								<MenuItem>Link 2</MenuItem>
								<MenuDivider />
								<MenuItem>Link 3</MenuItem>
							</MenuList>
						</Menu>
					</Flex> */}
					<IconButton
						bg={'primary.900'}
						_hover={{ bg: 'primary.900' }}
						size={'md'}
						icon={isOpen ? <CloseIcon color="white" /> : <HamburgerIcon color="white" />}
						aria-label={'Open Menu'}
						display={{ md: !isOpen ? 'none' : 'inherit' }}
						onClick={isOpen ? onClose : onOpen}
					/>
				</Flex>

				{isOpen ? (
					<Box pb={4}>
						<Stack as={'nav'} spacing={4}>
							{Links.map((link) => (
								<NavLink key={link}>{link}</NavLink>
							))}
						</Stack>
					</Box>
				) : null}
			</Box>
		</>
	);
}

function ThemeToggle() {
	const { colorMode, toggleColorMode } = useColorMode()
	return (<Button bg={useColorModeValue('primary.400', 'primary.900')} _hover={useColorModeValue('primary.900', 'primary.400')} onClick={toggleColorMode}>
		{colorMode === "light" ? <FaMoon /> : <FaSun />}
	</Button>)
}