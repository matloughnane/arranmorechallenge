import { useColorModeValue } from "@chakra-ui/color-mode";

export function getBadgeColour(type) {

	switch (type) {
		case "group":
			return useColorModeValue("primary.100", "primary.400")
		case "quarter":
			return useColorModeValue("primary.400", "primary.900")
		case "semi":
			return useColorModeValue("primary.100", "primary.400")
		case "final":
			return useColorModeValue("primary.400", "primary.900")
		default:
			return useColorModeValue("primary.100", "primary.400")
	}
}