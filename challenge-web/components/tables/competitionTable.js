// import Head from 'next/head'
import Link from 'next/link'
// import styles from '../styles/Home.module.css'

import React from 'react';

// import { getSortedPostsData } from '../lib/posts'
// import {
// 	Stack,
// 	Container,
// 	Heading,
// 	Box,
// 	Table,
// 	Thead,
// 	Tbody,
// 	Tfoot,
// 	Button,
// 	Badge,
// 	Tr,
// 	Th,
// 	Td,
// 	TableCaption,
// 	Text,
// 	Divider,
// 	Wrap,
// 	WrapItem,
// 	useColorModeValue
// } from '@chakra-ui/react'
// import NavBar from '../../components/common/navbar';
// import Footer from '../../components/common/footer';

// import { getBadgeColour } from '../../lib/utils';

// import { InfoIcon } from '@chakra-ui/icons';

// import { getArchivedYears, getYearParams, getHistoricalWinners, getYearData } from '../../lib/results'
// import Date from '../../components/date';
import SingleCompTable from './singleCompTable';


export default function CompetitionTable(props) {

	const groupResults = props.results.filter((r) => r.type === "group");
	const quarterResults = props.results.filter((r) => r.type === "quarter");
	const semiResults = props.results.filter((r) => r.type === "semi");
	const finalResults = props.results.filter((r) => r.type === "final");

	return (
		<>
			{/* <Heading mt={10} size={'md'}>Group Games</Heading> */}
			<SingleCompTable results={groupResults} title={"Group Games"} footer={true}/>
			<SingleCompTable results={quarterResults} title={"Quarter Finals"} footer={true}/>
			<SingleCompTable results={semiResults} title={"Semi Finals"} footer={false}/>
			<SingleCompTable results={finalResults} title={"The Final"} footer={false}/>
		</>
	)
}
