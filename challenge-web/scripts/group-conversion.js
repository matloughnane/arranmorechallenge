const groups = [
	{
	  "groupA": "Real Lypis ",
	  "groupB": "St Annes United ",
	  "groupC": "Midland Warriors ",
	  "groupD": "Finn Harps Youths"
	},
	{
	  "groupA": "Tory Celtic ",
	  "groupB": "Gallagher Tunnelling ",
	  "groupC": "F.C. Palatico ",
	  "groupD": "Stonecutters"
	},
	{
	  "groupA": "Glasgow Gaels ",
	  "groupB": "Arranmore ",
	  "groupC": "Clannad Celtic ",
	  "groupD": "Barhale"
	},
	{
	  "groupA": "Ballaly ",
	  "groupB": "The Rebels ",
	  "groupC": "F.C. Internazionale ",
	  "groupD": "Damo's Dream"
	}
  ];

const names = ["groupA", "groupB", "groupC", "groupD"];

let newGroup = [];

names.forEach((n) => {
	let tempObj = { "group": n };
	for (let index = 0; index < 4; index++) {
		const element = groups[index][n];
		// console.log(element);
		tempObj[index + 1] = element
	}
	newGroup.push(tempObj)
})

console.log(newGroup);