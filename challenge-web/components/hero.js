import {
	Button,
	Flex,
	Heading,
	Image,
	Stack,
	Text,
	useBreakpointValue,
	useColorModeValue
} from '@chakra-ui/react';

import { FaAngleDoubleRight } from 'react-icons/fa';
// 
import Link from 'next/link'
// import Image from 'next/image'

export default function HomePageHero() {

	return (
		<Stack minH={'80vh'} direction={{ base: 'column', md: 'row' }}>
			<Flex p={8} flex={1} align={'center'} justify={'center'}>
				<Stack spacing={6} w={'full'} maxW={'lg'}>
					<Heading fontSize={{ base: '3xl', md: '4xl', lg: '5xl' }}>
						<Text
							as={'span'}
							position={'relative'}
							_after={{
								content: "''",
								width: 'full',
								height: useBreakpointValue({ base: '15%', md: '20%' }),
								position: 'absolute',
								bottom: 1,
								left: 0,
								bg: 'primary.100',
								zIndex: -1,
							}}>
							Arranmore's
			  			</Text>
						<br />{' '}
						<Text color={useColorModeValue('primary.400', 'primary.100')} as={'span'}>
							June Bank Holiday
			  			</Text>{' '}
						<Text color={useColorModeValue('primary.400', 'primary.100')} as={'span'}>
							Weekend
			  			</Text>{' '}
					</Heading>
					<Text fontSize={{ base: 'md', lg: 'lg' }} color={useColorModeValue('grey.600', 'grey.400')} >
						The Arranmore Challenge is an annual 7-a-side football tournament held every June Bank Holiday weekend. 16 teams compete over the 2 days for the cup and bragging rights.
					</Text>
					<Stack direction={{ base: 'column', md: 'row' }} spacing={4}>
						<Link href="/about">
						<Button
							rounded={'lg'}
							rightIcon={<FaAngleDoubleRight />}
							bg={'primary.400'}
							color={'white'}
							_hover={{
								bg: 'primary.100',
							}}>
							About
			  			</Button>
						</Link>
						{/* <Link href="/live">
							<Button
								rightIcon={<FaAngleDoubleRight />}
								rounded={'lg'} _hover={{
									bg: 'primary.100',
									color: 'white'
								}}>Live Results
							</Button>
						</Link> */}
					</Stack>
				</Stack>
			</Flex>
			<Flex flex={1}>
				{/* <Image
					alt={'Login Image'}
					objectFit={'cover'}
					src={
						'https://images.unsplash.com/photo-1527689368864-3a821dbccc34?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80'
					}
				/> */}
				<Image
					layout="fill"
					// width={4000}
					// height={2730}
					objectFit="cover"
					src="/images/backgrounds/pitch.jpg" />
			</Flex>
		</Stack>
	);
}