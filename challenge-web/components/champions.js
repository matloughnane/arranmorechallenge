import {
	Button,
	Box,
	Flex,
	Heading,
	Image as CImage,
	Stack,
	Text,
	useBreakpointValue,
	useColorModeValue
} from '@chakra-ui/react';
import Link from 'next/link';

import { FaAngleDoubleRight } from 'react-icons/fa';

// import Image from 'next/image'

export default function HomePageChampionsHero() {

	return (
		<Stack minH={'70vh'} direction={{ base: 'column', md: 'row' }}>
			<Flex flex={1}>
				{/* <Image
					alt={'Login Image'}
					objectFit={'cover'}
					src={
						'https://images.unsplash.com/photo-1527689368864-3a821dbccc34?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80'
					}
				/> */}
				{/* <Box w={500} color="blue"> */}
				<CImage
					layout="fill"
					objectFit="cover"
					// width={800}
					// height={600}
					src="/images/backgrounds/2023_winners.jpeg" />
				{/* </Box> */}
			</Flex>
			<Flex p={8} flex={1} align={'center'} justify={'center'}>
				<Stack spacing={6} w={'full'} maxW={'lg'}>
					<Heading fontSize={{ base: '3xl', md: '4xl', lg: '5xl' }}>
						<Text
							as={'span'}
							position={'relative'}
							_after={{
								content: "''",
								width: 'full',
								height: useBreakpointValue({ base: '15%', md: '20%' }),
								position: 'absolute',
								bottom: 1,
								left: 0,
								bg: 'primary.100',
								zIndex: -1,
							}}>
							The All Backs
			  			</Text>
						<br />{' '}
						<Text color={useColorModeValue('primary.400', 'primary.100')} as={'span'}>
							Current Challenge
			  			</Text>{' '}
						<Text color={useColorModeValue('primary.400', 'primary.100')} as={'span'}>
							Champs
			  			</Text>{' '}
					</Heading>
					<Text fontSize={{ base: 'md', lg: 'lg' }} color={useColorModeValue('grey.600', 'grey.400')}>
						The All Backs are the current champs having won the competition in 2023.
					</Text>
					<Stack direction={{ base: 'column', md: 'row' }} spacing={4}>
						<Link href="/results">
							<Button rightIcon={<FaAngleDoubleRight />}
								rounded={'lg'}
								bg={'primary.400'}
								color={'white'}
								_hover={{
									bg: 'primary.100',
								}}>
								Tournament Results
			  			</Button>
						</Link>
					</Stack>
				</Stack>
			</Flex>

		</Stack>
	);
}