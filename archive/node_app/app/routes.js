// app/routes.js
module.exports = function(app, passport) {

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/guest', function(req, res) {
        res.render('index.ejs'); // load the index.ejs file
    });

    // CATCH ALL FOR USERS NOT LOGGED IN
    app.get('/', isLoggedIn, function(req, res) {
        res.render('live.ejs', {
            user : req.user // get the user out of session and pass to template
        });
    });

    // TEST PROFILE
    app.get('/my-profile', isLoggedIn, function(req, res) {
        res.render('my-profile.ejs', {
            user : req.user // get the user out of session and pass to template
        });
    });

    // TEST PROFILE
    // app.get('/profile', isLoggedIn, function(req, res) {
    //     res.render('profile.ejs', {
    //         user : req.user // get the user out of session and pass to template
    //     });
    // });
    

    // =====================================
    // LOGIN ===============================
    // =====================================
    // show the login form
    app.get('/login', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('login.ejs', { message: req.flash('loginMessage') }); 
    });

    // process the login form
    // app.post('/login', do all our passport stuff here);

    // =====================================
    // SIGNUP ==============================
    // =====================================
    // show the signup form
    app.get('/signup', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });

    // process the signup form
    // app.post('/signup', do all our passport stuff here);

    // process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));


    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));




    // POST FROM MOBILE APP
    app.post('/mobileapp/signup', passport.authenticate('local-signup', {
        // var username = req.body.username
        successRedirect : '/mobileapp/successSignUp',
        failureRedirect : '/mobileapp/failureSignUp'
        // failureFlash : true
        // app.post('/login',
        // passport.authenticate('local'),
        // function(req, res) {
        //   // If this function gets called, authentication was successful.
        //   // `req.user` contains the authenticated user.
        //   res.redirect('/users/' + req.user.username);
        // });

        // successRedirect : '/', // redirect to the secure profile section
        // failureRedirect : '/login', // redirect back to the signup page if there is an error
        // failureFlash : true // allow flash messages
    }));

    app.get('/mobileapp/successSignUp', function(req, res) {
        // render the page and pass in any flash data if it exists
        // var reqUsername = req.params.username
        // var filter = {"username": reqUsername};

        // db.collection('users').find(filter).toArray( function (err, result) {
        //     if (err) return res.send(err);
        //     // res.send(result)
        //     // res.send(200);
        //     // console.log(result[0])
        //     res.json({"response": result[0]});
        // })
        res.json({"response": "OK"})
    });

    app.get('/mobileapp/signup/details/:username', function(req, res) {
        // render the page and pass in any flash data if it exists
        var reqUsername = req.params.username
        var filter = {"local.username": reqUsername};

        db.collection('users').find(filter, {"local.password": 0}).toArray( function (err, result) {
            if (err) return res.send(err);
            // res.send(result)
            // res.send(200);
            // console.log(result[0])
            res.json({"response": result[0]});
        })
        // res.json({"response": "OK"})
    });

    app.get('/mobileapp/failureSignUp', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.json({"response": "FAILURE"})
    });


    // POST FROM MOBILE APP - LOGIN
    // 
    app.post('/mobileapp/login', passport.authenticate('local-login', {
        // var username = req.body.username
        successRedirect : '/mobileapp/successLogin',
        failureRedirect : '/mobileapp/failureLogin'
    }));

    app.get('/mobileapp/successLogin', function(req, res) {
        // render the page and pass in any flash data if it exists
        // var reqUsername = req.params.username
        // var filter = {"username": reqUsername};

        // db.collection('users').find(filter).toArray( function (err, result) {
        //     if (err) return res.send(err);
        //     // res.send(result)
        //     // res.send(200);
        //     // console.log(result[0])
        //     res.json({"response": result[0]});
        // })
        res.json({"response": "OK"})
    });

    app.get('/mobileapp/login/details/:email', function(req, res) {
        // render the page and pass in any flash data if it exists
        var reqEmail = req.params.email
        var filter = {"local.email": reqEmail};

        console.log(filter)

        db.collection('users').find(filter, {"local.password": 0}).toArray( function (err, result) {
            if (err) return res.send(err);
            // res.send(result)
            // res.send(200);
            // console.log(result[0])
            res.json({"response": result[0]});
        })
        // res.json({"response": "OK"})
    });

    app.get('/mobileapp/failureLogin', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.json({"response": "FAILURE"})
    });

    // =====================================
    // LIVE SECTION =====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/', isLoggedIn, function(req, res) {
        // console.log(req.user);
        res.render('live.ejs', {
            user : req.user // get the user out of session and pass to template
        });
    });

    // =====================================
    // ADMIN SECTION =====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/admin', isLoggedIn, function(req, res) {
        // console.log(req.user);
        res.render('results.ejs', {
            user : req.user // get the user out of session and pass to template
        });
    });

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/guest');
    });


    // =====================================
    // TWITTER ROUTES ======================
    // =====================================
    // route for twitter authentication and login
    app.get('/auth/twitter', passport.authenticate('twitter'));

    // handle the callback after twitter has authenticated the user
    app.get('/auth/twitter/callback',
        passport.authenticate('twitter', {
            successRedirect : '/',
            failureRedirect : '/guest'
        }));

};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/guest');
}