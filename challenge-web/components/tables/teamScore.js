import {
	Text,
} from '@chakra-ui/react'

export default function TeamScore(props) {
	// console.log(props.pen);
	return (
		<Text>{props.score} {props.pen === 0 || props.pen === "" || props.pen === undefined ? "" : "p." + props.pen} {props.sd === 0 || props.sd === "" || props.sd === undefined ? "" : "sd." + props.sd}</Text>
	)
}